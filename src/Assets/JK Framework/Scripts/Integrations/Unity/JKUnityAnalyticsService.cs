﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Cloud.Analytics;

namespace JK.Integrations
{
	public class JKUnityAnalyticsService : ScriptableObject
	{
		public static void SendFacebookError(string error)
		{
			UnityAnalytics.CustomEvent("facebookError", new Dictionary<string, object>
			{
				{ "error", error }
			});
		}

		public static void SendFacebookConnection(string name, string gender)
		{
			UnityAnalytics.CustomEvent("facebookConnection", new Dictionary<string, object>
			{
				{ "name", name }
			});

			SendGender (gender);
		}

		static void SendGender(string genderName)
		{
			Gender gender = Gender.Unknown;
			if (genderName == "MALE") gender = Gender.Male;
			if (genderName == "FEMALE") gender = Gender.Female;

			UnityAnalytics.SetUserGender (gender);
		}

		public static void SendFacebookShare()
		{
			UnityAnalytics.CustomEvent("facebookShare", new Dictionary<string, object>
			{
			});
		}

		public static void SendFacebookInvite()
		{
			UnityAnalytics.CustomEvent("facebookInvite", new Dictionary<string, object>
			{
			});
		}

		public static void SendGameError(string error)
		{
			UnityAnalytics.CustomEvent("gameError", new Dictionary<string, object>
			{
				{ "error", error }
			});
		}

		public static void SendSceneLoad(string screenName)
		{
			UnityAnalytics.CustomEvent("sceneLoad", new Dictionary<string, object>
			{
				{ "screenName", screenName }
			});
		}

		public static void SendClueOpen()
		{
			UnityAnalytics.CustomEvent("clueOpen", new Dictionary<string, object>
			{
			});
		}

		public static void SendTutorialOpen()
		{
			UnityAnalytics.CustomEvent("tutorialOpen", new Dictionary<string, object>
			{
			});
		}

		public static void SendGameOver(int round, int bestScore, int currentScore, TimeSpan timePlaying)
		{
			UnityAnalytics.CustomEvent("gameOver", new Dictionary<string, object>
			{
				{ "round", round },
				{ "bestScore", bestScore },
				{ "currentScore", currentScore },
				{ "totalSecondsPlaying", (int)timePlaying.TotalSeconds }
			});
		}
	}
}