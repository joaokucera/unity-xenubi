﻿using UnityEngine;
using System.Collections;
using UnityEngine.Cloud.Analytics;

public class UnityAnalyticsIntegration : MonoBehaviour 
{
	void Start () 
	{		
		const string projectId = "bd14f9e4-5bf9-40c7-8de1-579e27dc6e72";
		UnityAnalytics.StartSDK (projectId);
	}
}