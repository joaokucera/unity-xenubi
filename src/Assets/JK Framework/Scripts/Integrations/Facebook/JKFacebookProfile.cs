using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Patterns;
using Xenubi;

namespace JK.Integrations
{
	public class JKFacebookProfile : JKSingleton<JKFacebookProfile> 
	{
		#region Fields

		private Image m_profilePictureImage;
		private Sprite m_defaultProfilePictureSprite;

		private Text m_profileNameText;

		#endregion

		#region Properties

		public static Transform TheTransform { get { return Instance.transform; } }

		private static Image TheProfilePictureImage
		{
			get
			{
				if (Instance.m_profilePictureImage == null)
				{
					Instance.m_profilePictureImage = Instance.GetComponentInChildren<Image> ();

					Instance.m_defaultProfilePictureSprite = Instance.m_profilePictureImage.sprite;
				}
				
				return Instance.m_profilePictureImage;
			}
		}

		private static Text TheProfileNameText 
		{
			get
			{
				
				if (Instance.m_profileNameText == null)
				{
					Instance.m_profileNameText = Instance.GetComponentInChildren<Text>();
				}

				return Instance.m_profileNameText;
			}
		}

		#endregion

		#region Methods

		public static void SetProfilePicture()
		{
			TheProfilePictureImage.sprite = JKFacebookUserInfo.ProfilePicture;
		}

		public static void SetProfileName()
		{
			TheProfileNameText.text = JKFacebookUserInfo.Name;
		}

		public static void SetProfileInGame()
		{
			TheProfileNameText.text = string.Format ("{0} ({1})", JKFacebookUserInfo.FirstName, JKFacebookUserInfo.PreviousScore);
		}

		public static void ClearData()
		{
			JKFacebookUserInfo.ProfilePicture = null;

			TheProfilePictureImage.sprite = Instance.m_defaultProfilePictureSprite;

			TheProfileNameText.text = Translator.CurrentTranslation.Facebook.Guest;
		}

		#endregion
	}
}