using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Patterns;
using JK.Effects;
using Xenubi;

namespace JK.Integrations
{
	public class JKFacebookInviteFriend : MonoBehaviour
	{
		#region Fields

		private Button m_inviteButton;
		private Text m_inviteButtonText;
		private string buttonTextString;

		#endregion

		#region Properties

		[HideInInspector] public string Identifier;

		#endregion

		#region Events

		void Start()
		{
			Initialize ();
		}

		#endregion

		#region Methods (Public)

		public void HideButton()
		{
			if (m_inviteButtonText == null || m_inviteButton == null)
			{
				Initialize();
			}

			m_inviteButton.interactable = false;
			m_inviteButton.gameObject.SetActive (false);
		}

		#endregion

		#region Methods (Private)

		void Initialize()
		{
			m_inviteButtonText = GetComponentInChildren<Text> ();
			m_inviteButtonText.text = JKFacebookInvite.InviteButton.text;

			m_inviteButton = GetComponent<Button>();
			m_inviteButton.onClick.AddListener(OnInvite);
		}

		void OnInvite()
		{
			JKTween.ShakeScaleTransform (m_inviteButton.transform);
			GameplayService.PlayClickButton ();

			JKFacebookInvite.OnInviteFriend (Identifier);
		}

		#endregion
	}
}