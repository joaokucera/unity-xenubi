﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Patterns;

namespace JK.Integrations
{
	public class JKFacebookConnection : JKSingleton<JKFacebookConnection> 
	{
		#region Fields

		private Text m_connectionButtonText;
		private string m_connectWord;
		private string m_disconnectWord;

		#endregion

		#region Methods (Public)

		public static void SetWords(string connect, string disconnect)
		{
			Instance.m_connectWord = connect;
			Instance.m_disconnectWord = disconnect;
		}

		public static void OnConnect()
		{
			JKFacebookService.onMethodVoidHandler += JKFacebookProfile.SetProfileName;
			JKFacebookService.onMethodVoidHandler += JKFacebookProfile.SetProfilePicture;

			JKFacebookService.UserInfoAPICall();
			JKFacebookService.ProfilePictureAPICall();
			JKFacebookService.QueryScoresAPICall();

			Instance.ConnnectButton ();
		}

		public static void OnDisconnect()
		{
			JKFacebookProfile.ClearData();

			Instance.DisconnnectButton ();
		}

		public static void ForceButtonConnected()
		{
			Instance.ConnnectButton ();
		}

		#endregion

		#region Methods (Private)

		void Initialize()
		{
			m_connectionButtonText = GetComponentInChildren<Text> ();
		}

		void ConnnectButton()
		{
			if (m_connectionButtonText == null)
			{
				Initialize();
			}
			
			m_connectionButtonText.text = m_disconnectWord;
		}

		void DisconnnectButton()
		{
			if (m_connectionButtonText == null)
			{
				Initialize();
			}

			m_connectionButtonText.text = m_connectWord;
		}

		#endregion
	}
}