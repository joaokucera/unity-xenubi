﻿using System;
using System.Collections;
using UnityEngine;
using JK.Effects;
using JK.Managment;
using JK.Patterns;
using System.Collections.Generic;

namespace JK.Integrations
{
	public class JKFacebookService : ScriptableObject
	{
		private const string AlreadyInitKey = "AlreadyInit";

		#region Delegates

		// METHOD EMPTY.
		public delegate void MethodVoidHandler();
		public static event MethodVoidHandler onMethodVoidHandler;
		private static void onMethodVoidHandler_Execute()
		{
			if (onMethodVoidHandler != null)
			{
				onMethodVoidHandler();
			}
		}

		// METHOD FLAG.
		public delegate void MethodFlagHandler(bool isInteractable);
		public static event MethodFlagHandler onMethodFlagHandler;
		private static void onMethodFlagHandler_Execute(bool isFlagged)
		{
			if (onMethodFlagHandler != null)
			{
				onMethodFlagHandler(isFlagged);
			}
		}

		// METHOD CONNECTON.
		public delegate void MethodConnectHandler();
		public static event MethodConnectHandler onMethodConnectHandler;
		private static void onMethodConnectHandler_Execute()
		{
			if (onMethodConnectHandler != null)
			{
				onMethodConnectHandler();
			}
		}

		// METHOD CONNECTON.
		public delegate void MethodDisconnectHandler();
		public static event MethodDisconnectHandler onMethodDisconnectHandler;
		private static void onMethodDisconnectHandler_Execute()
		{
			if (onMethodDisconnectHandler != null)
			{
				onMethodDisconnectHandler();
			}
		}

		#endregion

		#region Methods (Public)

		public static void InitAPICall()
		{
			FB.Init (() => { Debug.Log("FB.Init"); }, OnHideUnity);
		}
		
		public static void ConnectionAPICall()
		{
			if (!FB.IsLoggedIn)
			{
				OnLogin();
			}
			else
			{
				OnLogout();
			}
		}
		
		public static void UserInfoAPICall()
		{
			FB.API("/me?fields=id,first_name,name,gender,friends.limit(100).fields(id,first_name,name)", Facebook.HttpMethod.GET, UserInfoCallback);
		}
		
		public static void ProfilePictureAPICall()
		{
			FB.API("/me/picture?type=large", Facebook.HttpMethod.GET, ProfilePictureCallback);
		}

		public static void ShareAPICall (string sharePictureUrl, string shareLinkName, string shareLinkCaption, string shareLink)
		{
			FB.Feed
			(
				picture: sharePictureUrl, 
				linkName: shareLinkName, 
				linkCaption: shareLinkCaption, 
				link: shareLink
			);
		}
		
		public static void InviteAPICall (string inviteTitle, string inviteMessage)
		{
			FB.AppRequest 
			(
				message: inviteMessage,
				title: inviteTitle
			);
		}

		public static void InviteFriendAPICall (string friendIdentifier, string inviteTitle, string messageFriend, string data)
		{
			if (friendIdentifier == null)
			{
				return;
			}
			
			string[] recipient = { friendIdentifier };
			
			FB.AppRequest
			(
				message: messageFriend,
				to: recipient,                                     
				filters : null,                                                                                         
				excludeIds : null,                                                                                                 
				maxRecipients : null,                                                                                              
				data: data,
				title: inviteTitle
			);                                                                                                                                                                                                                         
		}

		public static void QueryScoresAPICall()
		{
			FB.API("/app/scores?fields=score,user.limit(100)", Facebook.HttpMethod.GET, QueryScoresCallback);
		}

		public static void SetScoresAPICall (int score)
		{
			var scoresData = new Dictionary<string, string> ();
			scoresData ["score"] = score.ToString();

			FB.API ("/me/scores", Facebook.HttpMethod.POST, delegate (FBResult result)
			{
				JKFacebookUtil.Log ("Score submit result: " + result.Text);
				
				QueryScoresAPICall ();
				
			}, scoresData);
		}

		#endregion

		#region Methods (Private)

 		public static void OnInitComplete()
		{
			OnLoggedIn(FB.IsLoggedIn);
		}
		
		static void OnHideUnity(bool isGameShows)
		{
			JKManagment.Paused = isGameShows;
		}
		
		static void OnLogin()
		{
			FB.Login ("email,publish_actions", LoginCallback);
		}
		
		static void OnLogout()
		{
			FB.Logout ();
			
			LogoutCallback ();
		}
		
		static void LoginCallback(FBResult result)
		{
			OnLoggedIn(FB.IsLoggedIn);
		}
		
		static void LogoutCallback()
		{
			OnLoggedIn(false);
		}
		
		static void OnLoggedIn(bool isLoggedIn)
		{
			if (isLoggedIn)
			{
				// Call DELEGATE Connect.
				onMethodConnectHandler_Execute();
			}
			else
			{
				// Call DELEGATE Disconnect.
				onMethodDisconnectHandler_Execute();
			}

			// Call DELEGATE Button Interactable.
			onMethodFlagHandler_Execute (isLoggedIn);
		}

		static void UserInfoCallback(FBResult result)
		{
			if (result.Error != null)
			{
				JKFacebookUtil.LogError("FB userInfo Error: " + result.Error);
				
				UserInfoAPICall();
				
				return;
			}
			
			JKFacebookUserInfo.FillUserInfo (result.Text);

			onMethodVoidHandler_Execute ();
		}
		
		static void ProfilePictureCallback(FBResult result)
		{
			if (result.Error != null)
			{
				JKFacebookUtil.LogError("FB ProfilePicture Error: " + result.Error);

				ProfilePictureAPICall();
				
				return;
			}

			JKFacebookUserInfo.FillPicture (result.Texture);

			onMethodVoidHandler_Execute ();
		}

		static void QueryScoresCallback(FBResult result)
		{
			if (result.Error != null)
			{
				JKFacebookUtil.LogError("FB Scores Error: " + result.Error);
				
				return;
			}

			JKFacebookUserInfo.FillScoresCollection(result.Text);
		}

		#endregion
	}
}