using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Patterns;
using Xenubi;

namespace JK.Integrations
{
	public class JKFacebookInvite : JKSingleton<JKFacebookInvite> 
	{
		#region Fields

		private Text m_inviteButtonText;
		private string m_inviteWord;

		public static Text InviteButton 
		{
			get
			{
				if (Instance.m_inviteButtonText == null)
				{
					Instance.m_inviteButtonText = Instance.GetComponentInChildren<Text> ();
				}

				return Instance.m_inviteButtonText;
			}
		}

		#endregion

		#region Methods (Public)
		
		public static void OnInvite()
		{
			var facebookTranslation = Translator.CurrentTranslation.Facebook;

			JKFacebookService.InviteAPICall (facebookTranslation.InviteTitle, facebookTranslation.InviteMessage);

			JKUnityAnalyticsService.SendFacebookInvite ();
		}

		public static void OnInviteFriend(string friendIdentifier)
		{
			var translation = Translator.CurrentTranslation;

			int previous = JKFacebookUserInfo.PreviousScore;
			string title, friendMessage;
			string data = "{\"challenge_score\": " + previous + " }";

			JKFacebookPlayerScore friend = JKFacebookUserInfo.GetFriendPlayerScore(friendIdentifier);

			if (previous > friend.Score)
			{
				title = string.Format(translation.Facebook.InviteChallenge, friend.Name);

				string previousText = translation.Game.ResultPanel.Score;
				if (previous > 1) previousText += "s";

				int diff = previous - friend.Score;
				string diffText = translation.Game.ResultPanel.Score;
				if (diff > 1) diffText += "s";

				friendMessage = string.Format(translation.Facebook.InvitePoints, previous, previousText, diff, diffText);
			}
			else
			{
				title = string.Format(translation.Facebook.InviteText, friend.Name);

				string previousText = translation.Game.ResultPanel.Score;
				if (previous > 1) previousText += "s";

				friendMessage = string.Format(translation.Facebook.InviteResult, previous, previousText);
			}

			JKFacebookService.InviteFriendAPICall (friendIdentifier, title, friendMessage, data);

			JKUnityAnalyticsService.SendFacebookInvite ();
		}

		#endregion
	}
}