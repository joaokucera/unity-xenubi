﻿using System.Collections;
using UnityEngine;

namespace JK.Integrations
{
	public class JKFacebookPlayerScore
	{
		#region Variables

		public string Id;
		public string Name;
		public int Score;

		#endregion
	}
}