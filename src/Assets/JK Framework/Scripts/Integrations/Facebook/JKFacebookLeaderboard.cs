using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JK.Patterns;
using JK.Effects;
using Xenubi;

namespace JK.Integrations
{
	public class JKFacebookLeaderboard : JKSingleton<JKFacebookLeaderboard> 
	{
		#region Variables (Inspector)

		[SerializeField] private GameObject content;
		[SerializeField] private GameObject friendScorePrefab;

		[SerializeField] private Sprite[] spriteRows;
		[SerializeField] private Color playerNameColor;

		#endregion

		#region Methods

		public static void SetLeaderboard (List<JKFacebookPlayerScore> playerScores)
		{
			if (!Instance.content)
			{
				return;
			}

			Instance.StartCoroutine(Instance.CleanLeaderboard (playerScores));
		}

		IEnumerator CleanLeaderboard(List<JKFacebookPlayerScore> playerScores)
		{
			foreach (Transform entry in content.transform)
			{
				Destroy(entry.gameObject);
			}

			while (content.transform.childCount > 0) 
			{
				yield return null;
			}

			FillLeaderboard (playerScores);
		}

		void FillLeaderboard(List<JKFacebookPlayerScore> playerScores)
		{
			int position = 0;

			foreach (var score in playerScores)
			{
				var friendScore = GameObject.Instantiate(friendScorePrefab) as GameObject;
				friendScore.transform.SetParent(content.transform, false);

				// Row BACKGROUND.
				int index = position % 2 == 0 ? 0 : 1;
				friendScore.GetComponent<Image>().sprite = spriteRows[index];
				
				Image thisFriendAvatarImage = friendScore.transform.FindChild("Picture (Empty)").GetComponentInChildren<Image>();
				if (thisFriendAvatarImage)
				{
					FB.API("/" + score.Id + "/picture?type=large", Facebook.HttpMethod.GET, delegate(FBResult friendPic)
					{
						if (friendPic.Error != null)
						{
							JKFacebookUtil.Log("FB FriendPicture Error: " + friendPic.Error);
							
							return;
						}
						
						// Friend PICTURE.
						thisFriendAvatarImage.sprite = Sprite.Create(friendPic.Texture, new Rect(0, 0, friendPic.Texture.width, friendPic.Texture.height), Vector2.zero);
					});
				}

				// Friend POSITON.
				Text thisPositionText = friendScore.transform.FindChild("Position (Empty)").GetComponentInChildren<Text>();
				thisPositionText.text = string.Format("{0}º", ++position);

				// Friend NAME.
				Text thisFriendNameText = friendScore.transform.FindChild("Name (Empty)").GetComponentInChildren<Text>();
				thisFriendNameText.text = score.Name.ToUpper();
				
				// Friend SCORE.
				Text thisFriendScoreText = friendScore.transform.FindChild("Score (Empty)").GetComponentInChildren<Text>();
				string scoreText = Translator.CurrentTranslation.Leaderboard.Score;
				if (score.Score > 1) scoreText += "S";
				thisFriendScoreText.text = string.Format("{0} {1}", score.Score.ToString(), scoreText);

				// Friend BUTTON Invite.
				var thisInviteButton = friendScore.transform.FindChild("Invite (Empty)").GetComponentInChildren<JKFacebookInviteFriend>();

				// É o PLAYER!!!
				if (score.Id == JKFacebookUserInfo.Identifier)
				{
					thisInviteButton.HideButton();
					thisPositionText.color = playerNameColor;

					GameplayService.PlayOpenPanel();
					JKTween.PunchTransform(friendScore.transform, Vector2.one / 4, 1f);
				}
				else
				{
					thisInviteButton.Identifier = score.Id;
				}
			}
		}

		#endregion
	}
}