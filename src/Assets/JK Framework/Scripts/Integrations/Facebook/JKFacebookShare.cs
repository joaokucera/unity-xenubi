using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Patterns;
using System;
using Xenubi;

namespace JK.Integrations
{
	public class JKFacebookShare : JKSingleton<JKFacebookShare> 
	{
		#region Fields

		private Text m_shareButtonText;
		private string m_shareLink = string.Format("http://apps.facebook.com/{0}/?challenge_hrag={1}", FB.AppId, (FB.IsLoggedIn ? FB.UserId : "guest"));
		
		#endregion

		#region Events
		
		void Start()
		{
			m_shareButtonText = GetComponentInChildren<Text> ();
			m_shareButtonText.text = Translator.CurrentTranslation.Facebook.Share;
		}
		
		#endregion
		
		#region Methods
		
		public static void OnShare()
		{	
			var facebookTranslation = Translator.CurrentTranslation.Facebook;

			JKFacebookService.ShareAPICall (facebookTranslation.ShareImageUrl, "XeNUBi", facebookTranslation.ShareCaption, Instance.m_shareLink);

			JKUnityAnalyticsService.SendFacebookShare ();
		}

		public static void OnShareResult(int currentScore)
		{
			var translation = Translator.CurrentTranslation;

			int previous = JKFacebookUserInfo.PreviousScore;

			int score = currentScore > previous ? currentScore : previous;
			string scoreText = translation.Game.ResultPanel.Score;
			if (score > 1) scoreText += "s";

			string linkCaption = string.Format(translation.Facebook.ShareResult, score, scoreText);

			JKFacebookService.ShareAPICall (translation.Facebook.ShareImageUrl, translation.Facebook.ShareTitle, linkCaption, Instance.m_shareLink);

			JKUnityAnalyticsService.SendFacebookShare ();
		}

		#endregion
	}
}