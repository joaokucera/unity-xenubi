﻿using System.Collections;
using UnityEngine;
using JK.Patterns;

namespace JK.Managment
{
	public class JKManagment : JKSingleton<JKManagment> 
	{
		#region Fields

		private Camera m_camera;

		#endregion

		#region Properties

		public static bool IntroShowOff;
		public static bool TutorialShowOff;

		public static bool Paused
		{
			get { if (Time.timeScale == 0f) { return true; } else { return false; } }
			set { if (value) { Time.timeScale = 0f; } else { Time.timeScale = 1f; } }
		}
		
		public static Camera MainCamera
		{
			get { if (Instance.m_camera == null) { Instance.m_camera = Camera.main; } return Instance.m_camera; }
		}
		
		#endregion
	}
}