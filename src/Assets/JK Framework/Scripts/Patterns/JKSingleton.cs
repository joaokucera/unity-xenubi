﻿using System.Collections;
using UnityEngine;

namespace JK.Patterns
{
	public class JKSingleton<T> : MonoBehaviour where T : MonoBehaviour
	{
		#region Fields

		protected static T m_instance;

		#endregion

		#region Properties

		protected static T Instance 
		{
			get
			{
				if (m_instance == null)
				{
					m_instance = FindObjectOfType<T>();
				}

				if (m_instance == null)
				{
					string error = string.Format("An instance of {0} is needed in the scene, but there is none.", typeof(T));

					Debug.LogError(error);
				}

				return m_instance;
			}
		}

		#endregion
	}
}