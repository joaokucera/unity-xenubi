﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Patterns;

namespace JK.Effects
{
	public class JKScreenFade : JKSingleton<JKScreenFade>
	{
		#region Variables (Inspector)

		[SerializeField] private Image fadeImage;
		[SerializeField] private float fadeSpeed = 1f;

		#endregion

		#region Properties

		public static bool FadeImageEnabled { set { Instance.fadeImage.enabled = value; } }

		#endregion

		#region Events

		void Awake()
		{
			if (fadeImage == null)
			{
				Debug.LogError("There is no fade image assigned.");

				return;
			}

			fadeImage.rectTransform.localScale = new Vector3 (Screen.width, Screen.height);
			fadeImage.color = Color.black;
		}

		#endregion

		#region Methods (Public)

		public static void OpenScene()
		{
			Instance.StartCoroutine (Instance.StartScene());
		}

		public static void CloseScene(Action callback)
		{
			Instance.StartCoroutine (Instance.EndScene(callback));
		}

		#endregion

		#region Methods (Private)

		IEnumerator StartScene()
		{
			while (fadeImage.color.a > 0.05f)
			{
				FadeToClear();

				yield return null;
			}

			fadeImage.color = Color.clear;
			fadeImage.enabled = false;
		}

		IEnumerator EndScene(Action callback)
		{
			fadeImage.enabled = true;

			while (fadeImage.color.a < 0.95f) 
			{
				FadeToBlack();

				yield return null;
			}

			callback ();
		}

		void FadeToClear ()
		{
			fadeImage.color = Color.Lerp(fadeImage.color, Color.clear, fadeSpeed * Time.deltaTime);
		}

		void FadeToBlack ()
		{
			fadeImage.color = Color.Lerp(fadeImage.color, Color.black, fadeSpeed * Time.deltaTime);
		}

		#endregion
	}
}