﻿using System.Collections;
using UnityEngine;
using JK.Patterns;

namespace JK.Effects
{
    public class JKScreenShake : JKSingleton<JKScreenShake>
    {
        #region Fields

		private Camera m_camera;
        private Vector3 m_originPosition;
        private Quaternion m_originRotation;
        private float m_shakeIntensity = 0;
        private float m_shakeDecay = 0.001f;
        private float m_shakeCoefIntensity = 0.01f;

        #endregion

		#region Events

		void Start()
		{
			m_camera = Camera.main;
		}

		#endregion

		#region Methods (Public)

        public static void Shake(float multiplier)
        {
			Instance.m_originPosition = Instance.m_camera.transform.position;
			Instance.m_originRotation = Instance.m_camera.transform.rotation;

			Instance.m_shakeIntensity = Instance.m_shakeCoefIntensity;

			Instance.StartCoroutine (Instance.UpdateShake (multiplier));
        }

		#region Methods (Private)

		#endregion

        IEnumerator UpdateShake(float multiplier)
        {
            while (m_shakeIntensity > 0)
            {
				m_camera.transform.position = m_originPosition + Random.insideUnitSphere * m_shakeIntensity;

				m_camera.transform.rotation = new Quaternion
                (
                    m_originRotation.x + Random.Range(-m_shakeIntensity, m_shakeIntensity) * multiplier,
                    m_originRotation.y + Random.Range(-m_shakeIntensity, m_shakeIntensity) * multiplier,
                    m_originRotation.z + Random.Range(-m_shakeIntensity, m_shakeIntensity) * multiplier,
                    m_originRotation.w + Random.Range(-m_shakeIntensity, m_shakeIntensity) * multiplier
                );

                m_shakeIntensity -= m_shakeDecay;

				m_originPosition = m_camera.transform.position;

                yield return null;
            }

			m_camera.transform.position = m_originPosition;
			m_camera.transform.rotation = m_originRotation;
        }

        #endregion
    }
}