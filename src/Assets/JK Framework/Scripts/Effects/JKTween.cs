﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using JK.Managment;
using JK.Patterns;

namespace JK.Effects
{
	public class JKTween : ScriptableObject
	{
		private static bool m_pause;
		public static bool Pause 
		{
			set 
			{
				m_pause = value;

				if (!m_pause)
				{
					DOTween.PlayAll();
				}
				else
				{
					DOTween.PauseAll();
				}
			}
		}

		static void ExecuteCallback(Action callback)
		{
			if (callback != null)
			{
				callback();
			}
		}

		public static void ShowCanvasGroup(CanvasGroup canvasGroup, float duration, Action callback = null)
		{
			canvasGroup.alpha = 0;

			var delay = JKManagment.IntroShowOff ? 0 : duration;
			canvasGroup.DOFade (1, duration).SetDelay(delay).OnComplete(() => ExecuteCallback(callback));
		}

		public static IEnumerator HideCanvasGroup(CanvasGroup canvasGroup, float duration, int levelIndex)
		{
			canvasGroup.alpha = 1;

			Tween myTween = canvasGroup.DOFade (0, duration);
			yield return myTween.WaitForCompletion ();

			Application.LoadLevel (levelIndex);
		}

		public static void HideCanvasGroup(CanvasGroup canvasGroup, float duration)
		{
			canvasGroup.alpha = 1;
			
			canvasGroup.DOFade (0, duration);
		}

		public static void RotateTransform(Transform tr, float duration, Action callback = null)
		{
			tr.rotation = Quaternion.Euler (0, 0, 180);

			tr.DORotate(Vector3.zero, duration, RotateMode.FastBeyond360).OnComplete(() => ExecuteCallback(callback));
		}

		public static void ScaleTransform(Transform tr, Vector3 originalScale, float endValue, float duration, Action callback = null)
		{
			tr.localScale = originalScale;

			tr.DOScale(endValue, duration).OnComplete(() => ExecuteCallback(callback));
		}

		public static Tween PunchTransform(Transform tr, Vector3 punch, float duration, Action callback = null)
		{
			return tr.DOPunchScale (punch, duration, 1).OnComplete(() => ExecuteCallback(callback));
		}

		public static void PunchTransformWithDelay(Transform tr, Vector3 punch, float duration, float delay, Action startCallback = null, Action completeCallback = null)
		{
			tr.DOPunchScale (punch, duration, 1)
				.SetDelay(delay)
				.OnStart(() => ExecuteCallback(startCallback))
				.OnComplete(() => ExecuteCallback(completeCallback));
		}

		public static void ShakeScaleTransform(Transform tr, Action callback = null)
		{
			tr.DOShakeScale (1, 0.25f, 1).OnComplete(() => ExecuteCallback(callback));
		}

		public static void ShakeScaleTransformWithDelay(Transform tr, float delay, Action startCallback = null, Action completeCallback = null)
		{
			tr.DOShakeScale (1, 0.5f, 1, 0)
				.SetDelay(delay)
				.OnStart(() => ExecuteCallback(startCallback))
				.OnComplete(() => ExecuteCallback(completeCallback));
		}

		public static void AlphaColorImage(Image img, float endValue, bool toShow, float duration, Action callback = null)
		{
			Tweener tweener = DOTween.ToAlpha (() => img.color, x => img.color = x, endValue, duration);

			if (toShow)
			{
				tweener.OnStart(() => img.enabled = true).OnComplete (() => ExecuteCallback (callback));
			}
			else
			{
				tweener.OnComplete(() => 
				{ 
					img.enabled = false; 
					ExecuteCallback (callback); 
				});
			}
		}

		public static void ColorImage(Image img, Color color, float duration, Action callback = null)
		{
			img.DOColor (color, duration).OnComplete (() => ExecuteCallback (callback));
		}

		public static void LocalMoveTransform(Transform tr, Vector3 originalPosition, Vector3 endValue, float duration, Action callback = null)
		{
			tr.localPosition = originalPosition;
			
			tr.DOLocalMove (endValue, duration).OnComplete (() => ExecuteCallback (callback));
		}

		public static void LocalMoveXTransform(Transform tr, Vector3 originalPosition, float endValue, float duration, Action callback = null)
		{
			tr.localPosition = originalPosition;

			tr.DOLocalMoveX (endValue, duration).OnComplete (() => ExecuteCallback (callback));
		}

		public static void LocalMoveXTransform(Transform tr, float originalPositionX, float endValue, float duration, Action callback = null)
		{
			Vector3 pos = tr.localPosition;
			pos.x = originalPositionX;
			tr.localPosition = pos;
			
			tr.DOLocalMoveX (endValue, duration).OnComplete (() => ExecuteCallback (callback));
		}

		public static void LocalMoveYTransform(Transform tr, float originalPositionY, float endValue, float duration, Action callback = null)
		{
			Vector3 pos = tr.localPosition;
			pos.y = originalPositionY;
			tr.localPosition = pos;
			
			tr.DOLocalMoveY (endValue, duration).OnComplete (() => ExecuteCallback (callback));
		}

		public static void FadeText(Text txt, int startValue, int endValue, float duration, Action callback = null)
		{
			txt.DOFade (startValue, 0).OnComplete(() =>
			{
				txt.DOFade (endValue, duration).OnComplete (() => ExecuteCallback (callback));
			});
		}

		public static void FadeImage(Image img, int startValue, int endValue, float duration)
		{
			img.DOFade (startValue, 0).OnComplete(() =>
			{
				img.DOFade (endValue, duration);
			});
		}
	}
}