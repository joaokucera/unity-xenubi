﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JK.Patterns;
using System;
using Random = UnityEngine.Random;

namespace JK.Effects
{
	public class JKSoundManager : JKSingleton<JKSoundManager>
	{
		#region Fields

		private Dictionary<string, AudioClip> m_soundDictionary = new Dictionary<string, AudioClip>();

		private AudioSource m_musicSource;
		private AudioSource m_sfxSource;

		private const float MaxMusicVolume = 0.5f;
		private const float MinMusicVolume = 0.1f;

		#endregion

		#region Variables (Inspector)

		[SerializeField] private AudioClip[] sfxClips;

		#endregion

		#region Properties

		public static bool MusicEnabled = true;
		public static bool SoundEffectsEnabled = true;

		private AudioSource MusicSource
		{
			get
			{
				if (m_musicSource == null)
				{
					m_musicSource = transform.FindChild ("Music Source").GetComponent<AudioSource> ();
					m_musicSource.playOnAwake = false;
					m_musicSource.loop = true;
				}

				return m_musicSource;
			}
		}

		private AudioSource SfxSource
		{
			get
			{
				if (m_sfxSource == null)
				{
					m_sfxSource = transform.FindChild ("Sound Effect Source").GetComponent<AudioSource> ();
					m_sfxSource.playOnAwake = false;
					m_sfxSource.loop = false;
				}
				
				return m_sfxSource;
			}
		}

		#endregion

		#region Events

		void Awake()
		{
			if (m_instance == null)
			{
				m_instance = this;
				
				DontDestroyOnLoad(gameObject);

				CreateSoundsDictionary ();
			}
			else
			{
				Destroy(gameObject);
			}
		}
		
		#endregion

		#region Methods (Public)

		public static void PlayMusic(AudioClip newMusic)
		{
			if (Instance.MusicSource.clip == null)
			{
				Instance.StartCoroutine(Instance.PlayingMusic(newMusic));
			}
			else if (!Instance.MusicSource.clip.name.Equals(newMusic.name))
			{
				Instance.StartCoroutine(Instance.StoppingMusic (newMusic));
			}
		}

		public static void PlayMusicImmediate(AudioClip newMusic)
		{
			Instance.MusicSource.Stop ();
			
			Instance.StartCoroutine (Instance.PlayingMusic (newMusic));
		}

		public static void PlaySoundEffect(string clipName)
		{
			Instance.PlayingSoundEffect (clipName);
		}

		public static void RandomSoundEffect(params string[] clipNamesArray)
		{
			int index = Random.Range (0, clipNamesArray.Length);
			string clipName = clipNamesArray [index];

			Instance.PlayingSoundEffect (clipName);
		}

		public static void ActiveMusic(Action callback)
		{
			MusicEnabled = !MusicEnabled;
			
			if (MusicEnabled)
			{
				Instance.ContinueMusic ();
			}
			else
			{
				Instance.PauseMusic();
			}
			
			callback();
		}
		
		public static void ActiveSoundEffects(Action callback)
		{
			SoundEffectsEnabled = !SoundEffectsEnabled;
			
			callback();
		}
		
		public static void ActiveMusicFade(Action callback, float waitTime)
		{
			Instance.StartCoroutine(Instance.MusicFade (callback, waitTime));
		}

		#endregion

		#region Methods (Private)

		IEnumerator MusicFade(Action callback, float waitTime)
		{
//			while (MusicSource.volume > MinMusicVolume)
//			{
//				MusicSource.volume -= Time.deltaTime;
//				yield return null;
//			}

			//MusicSource.volume = MinMusicVolume;
			MusicSource.Stop ();

			callback ();
			yield return new WaitForSeconds (waitTime);

			MusicSource.Play ();
			//MusicSource.volume = MaxMusicVolume;

//			while (MusicSource.volume < MaxMusicVolume)
//			{
//				MusicSource.volume += Time.deltaTime;
//				yield return null;
//			}
		}

		void CreateSoundsDictionary()
		{
			for (int i = 0; i < sfxClips.Length; i++) 
			{
				m_soundDictionary.Add(sfxClips[i].name, sfxClips[i]);
			}
		}

		void PauseMusic()
		{
			if (MusicSource.isPlaying)
			{
				MusicSource.Pause();
			}
		}
		
		void ContinueMusic()
		{
			if (!MusicSource.isPlaying)
			{
				MusicSource.Play();
			}
		}

		IEnumerator PlayingMusic(AudioClip newMusic)
		{
			MusicSource.clip = newMusic;
			MusicSource.volume = 0f;

			if (MusicEnabled)
			{
				MusicSource.Play();
			}
			
			while (MusicSource.volume < MaxMusicVolume)
			{
				MusicSource.volume += Time.deltaTime;
				yield return null;
			}
		}

		IEnumerator StoppingMusic(AudioClip newMusic)
		{
			while (MusicSource.volume > MinMusicVolume)
			{
				MusicSource.volume -= Time.deltaTime;
				yield return null;
			}

			MusicSource.Stop ();

			StartCoroutine (PlayingMusic (newMusic));
		}

		void PlayingSoundEffect (string clipName)
		{
			if (!SoundEffectsEnabled) 
			{
				return;
			}
			
			AudioClip originalClip;
			
			if (Instance.m_soundDictionary.TryGetValue (clipName, out originalClip)) 
			{
				Instance.MakeSoundEffect (originalClip);
			}
		}

		void MakeSoundEffect(AudioClip originalClip)
		{
			SfxSource.PlayOneShot(originalClip);
		}
		
		#endregion
	}
}