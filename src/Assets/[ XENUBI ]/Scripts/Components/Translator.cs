﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;
using JK.Effects;
using JK.Patterns;

namespace Xenubi
{
	public class Translator : JKSingleton<Translator>  
	{
		private int m_currentLanguage;
		private Dictionary<int, Translation> m_resourcesDictionary = new Dictionary<int, Translation>();
		private Dictionary<int, List<Element>> m_elementsDictionary = new Dictionary<int, List<Element>>();

		[SerializeField] private TextAsset[] m_xmlResources;
		[SerializeField] private TextAsset[] m_xmlElements;

		public static int CurrentLanguage { get { return Instance.m_currentLanguage; } }
		public static Translation CurrentTranslation { get { return Instance.m_resourcesDictionary[Instance.m_currentLanguage]; } }
		public static List<Element> CurrentElements { get { return Instance.m_elementsDictionary[Instance.m_currentLanguage]; } }

		void Awake()
		{
			if (m_instance == null)
			{
				m_instance = this;
				
				DontDestroyOnLoad(gameObject);
				
				CreateResourcesDictionary ();

				CreateElementsDictionary ();
			}
			else
			{
				Destroy(gameObject);
			}
		}

		public static void NextTranslation(Transform tr, Action callback)
		{
			GameplayService.PlayClickButton ();

			Instance.m_currentLanguage++;

			if (Instance.m_currentLanguage >= Instance.m_resourcesDictionary.Count)
			{
				Instance.m_currentLanguage = 0;
			}

			JKTween.ShakeScaleTransform (tr, callback);
		}

		void CreateResourcesDictionary()
		{
			for (int i = 0; i < m_xmlResources.Length; i++) 
			{
				m_resourcesDictionary.Add(i, CreateNewResource(m_xmlResources[i]));
			}
		}

		Translation CreateNewResource (TextAsset textAsset)
		{
			Translation translation;

			var serializer = new XmlSerializer(typeof(Translation));
			var buffer = Encoding.UTF8.GetBytes(textAsset.text);

			using (var stream = new MemoryStream(buffer))
			{
				translation = (Translation)serializer.Deserialize(stream);
			}

			return translation;
		}

		void CreateElementsDictionary()
		{
			for (int i = 0; i < m_xmlElements.Length; i++) 
			{
				m_elementsDictionary.Add(i, CreateNewElements(m_xmlElements[i]));
			}
		}

		List<Element> CreateNewElements(TextAsset textAsset)
		{
			XmlReader reader = XmlReader.Create (new StringReader (textAsset.text));
			
			List<Element> list = new List<Element> ();
			Element currentElement = null;
			
			while (reader.Read()) 
			{
				if (reader.IsStartElement("ChemistryElement"))
				{
					currentElement = new Element();
					list.Add(currentElement);
				}
				
				if (currentElement != null)
				{
					if (reader.IsStartElement("row"))
					{
						currentElement.Row = reader.ReadElementContentAsInt();
					}
					if (reader.IsStartElement("column"))
					{
						currentElement.Column = reader.ReadElementContentAsInt();
					}
					if (reader.IsStartElement("name"))
					{
						currentElement.Name = reader.ReadElementContentAsString();
					}
					if (reader.IsStartElement("symbol"))
					{
						currentElement.Symbol = reader.ReadElementContentAsString();
					}
					if (reader.IsStartElement("atomicNumber"))
					{
						currentElement.AtomicNumber = reader.ReadElementContentAsInt();
					}
					if (reader.IsStartElement("atomicMass"))
					{
						currentElement.AtomicMass = reader.ReadElementContentAsFloat();
					}
					if (reader.IsStartElement("atomicRadius"))
					{
						currentElement.AtomicRadius = reader.ReadElementContentAsFloat();
					}
					if (reader.IsStartElement("eletroNegativity"))
					{
						currentElement.ElectronNegativity = reader.ReadElementContentAsFloat();
					}
					if (reader.IsStartElement("ionizationEnergy"))
					{
						currentElement.IonizationEnergy = reader.ReadElementContentAsFloat();
					}
					if (reader.IsStartElement("eletroAfinidade"))
					{
						currentElement.ElectronAffinity = reader.ReadElementContentAsFloat();
					}
					if (reader.IsStartElement("density"))
					{
						currentElement.Density = reader.ReadElementContentAsFloat();
					}
					if (reader.IsStartElement("PF"))
					{
						currentElement.PF = reader.ReadElementContentAsString();
					}
					if (reader.IsStartElement("fusionPoint"))
					{
						currentElement.MeltingPoint = reader.ReadElementContentAsFloat();
					}
					if (reader.IsStartElement("boilingPoint"))
					{
						currentElement.BoilingPoint = reader.ReadElementContentAsString();
					}
					if (reader.IsStartElement("abundance"))
					{
						currentElement.Abundance = reader.ReadElementContentAsString();
					}
				}
			}
			
			return list;
		}
	}
}