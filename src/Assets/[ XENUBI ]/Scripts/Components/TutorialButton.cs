﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using JK.Effects;
using System;
using UnityEngine.Events;

public class TutorialButton : MonoBehaviour 
{
	private Button m_btn;
	private Image m_img;
	private Text m_txt;

	void Start()
	{
		Initialize ();
	}

	void Initialize()
	{
		gameObject.SetActive (true);

		m_btn = GetComponent<Button> ();
		m_btn.enabled = false;

		m_img = GetComponent<Image> ();
		if (m_img == null) m_img = GetComponentInChildren<Image> ();
		m_img.enabled = false;

		m_txt = GetComponentInChildren<Text> ();
		if (m_txt != null) m_txt.enabled = false;
	}

	public void InteractWith(bool canInteract)
	{
		if (m_btn.interactable != canInteract)
		{
			m_btn.interactable = canInteract;

			m_img.color = canInteract ? Color.white : new Color(0.5f, 0.5f, 0.5f);
		}
	}

	public void Setup(UnityAction call)
	{
		if (m_btn == null || m_img == null || m_txt == null)
		{
			Initialize ();
		}

		m_btn.onClick.AddListener (call);
	}

	public void MoveIn(float from, TweenAxis axis, Action callback = null)
	{
		m_img.enabled = true;
		if (m_txt != null) m_txt.enabled = true;

		if (axis == TweenAxis.X)
		{
			JKTween.LocalMoveXTransform (transform, from, transform.localPosition.x, 1f, () => 
			{
				m_btn.enabled = true;

				if (callback != null)
				{
					callback();
				}
			});
		}
		else
		{
			JKTween.LocalMoveYTransform (transform, from, transform.localPosition.x, 1f, () => 
			{
				m_btn.enabled = true;
				
				if (callback != null)
				{
					callback();
				}
			});
		}
	}

	public void MoveOut(float to, TweenAxis axis, Action callback = null)
	{
		m_btn.enabled = false;

		if (axis == TweenAxis.X)
		{
			JKTween.LocalMoveXTransform (transform, transform.localPosition.x, to, 1f, () => 
			{
				m_img.enabled = false;
				if (m_txt != null) m_txt.enabled = false;

				if (callback != null)
				{
					callback();
				}
			});
		}
		else
		{
			JKTween.LocalMoveYTransform (transform, transform.localPosition.x, to, 1f, () => 
			{
				m_img.enabled = false;
				if (m_txt != null) m_txt.enabled = false;
				
				if (callback != null)
				{
					callback();
				}
			});
		}
	}
}