﻿public enum ButtonName
{
	Game,
	Settings,
	Leaderboard,
	Music,
	SoundEffect,
	HowToPlay,
	Credits,
	Menu,
	Connection, // Facebook
	Share, // Facebook
	Invite, // Facebook
	Pause,
	Close,
	Clue,
	Play,
	Reload,
	Previous,
	Next,
	Skip,
	Yes,
	No,
	Language
}

public enum ElementType
{
	AtomicRadius = 0,
	IonizationEnergy = 1,
	ElectronAffinity = 2,
	ElectronNegativity = 3,
	MeltingPoint = 4,
	Density = 5
}

public enum ScreenName
{
	Intro = 0,
	Menu = 1,
	Game = 2,
	Settings = 3,
	Leaderboard = 4,
	Quit = 5
}

public enum TweenAxis
{
	X,
	Y
}

public enum RoundChallenge
{
	OnlySymbol = 0,
	OnlySymbolAndName = 1,
	NoProperties = 2,
	RandomProperties = 3,
	WholeCard = 4
}

public enum MatchResult
{
	VictoryCombo,
	Victory,
	Defeat
}

public enum TranslatorLanguage
{
	PT_BR = 0,
	EN_US = 1,
	ES_ES = 2
}