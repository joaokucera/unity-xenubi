using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;	
using JK.Managment;
using Xenubi.Screens;

namespace Xenubi
{
	public class PanelPause : MonoBehaviour 
	{
		private bool m_timerIsPaused;

		private Button m_closeButton;
		private Button m_clueButton;
		private Button m_menuButton;
		private Button m_playButton;
		private Button m_reloadButton;
		private Button m_musicButton;
		private Button m_soundEffectButton;

		private GameScreen m_screen;
		private PanelBlur m_blurPanel;
		private PanelClue m_cluePanel;
		private PanelQuit m_quitPanel;

		[SerializeField] private AudioSettings audioSettings;

		public bool IsOpen { get; private set; }

		void Start()
		{
			Initialize ();
		}

		void Initialize ()
		{
			m_screen = FindObjectOfType<GameScreen> ();

			m_cluePanel = FindObjectOfType<PanelClue> ();
			m_blurPanel = FindObjectOfType<PanelBlur> ();
			m_quitPanel = FindObjectOfType<PanelQuit> ();

			transform.localScale = Vector3.zero;

			audioSettings.PrepareIcons ();
		}

		public void Setup ()
		{
			if (m_screen == null || m_cluePanel == null || m_blurPanel == null || m_quitPanel == null)
			{
				Initialize();
			}

			PrepareButtons ();
		}

		void PrepareButtons ()
		{
			Button[] buttons = GetComponentsInChildren<Button>();
			
			for (int i = 0; i < buttons.Length; i++)
			{
				if (buttons[i].name.Contains(ButtonName.Close.ToString()))
				{
					m_closeButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => ClosePanel(m_closeButton.transform));
				}
				else if (buttons[i].name.Contains(ButtonName.Clue.ToString()))
				{
					m_clueButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => m_cluePanel.OpenPanel(m_clueButton.transform));
				}
				else if (buttons[i].name.Contains(ButtonName.Menu.ToString()))
				{
					m_menuButton = buttons[i];
					
					buttons[i].onClick.AddListener(MenuButtonClick);
				}
				else if (buttons[i].name.Contains(ButtonName.Play.ToString()))
				{
					m_playButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => ClosePanel(m_playButton.transform));
				}
				else if (buttons[i].name.Contains(ButtonName.Reload.ToString()))
				{
					m_reloadButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => m_quitPanel.OpenPanel(ScreenName.Game, Translator.CurrentTranslation.QuitPanel.Content.ReloadMessage, m_reloadButton.transform));
				}
				else if (buttons[i].name.Contains(ButtonName.Music.ToString()))
				{
					m_musicButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => audioSettings.OnMusic(m_musicButton.transform));
				}
				else if (buttons[i].name.Contains(ButtonName.SoundEffect.ToString()))
				{
					m_soundEffectButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => audioSettings.OnSoundEffects(m_soundEffectButton.transform));
				}
			}
		}

		public void MenuButtonClick()
		{
			m_quitPanel.OpenPanel (ScreenName.Menu, Translator.CurrentTranslation.QuitPanel.Content.MenuMessage, m_menuButton.transform);
		}

		public void OpenPanel(Transform tr, bool timerIsPaused)
		{
			IsOpen = true;

			m_timerIsPaused = timerIsPaused;

			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayOpenPanel ();

			m_blurPanel.OpenPanel ();
			JKTween.ScaleTransform (transform, Vector3.zero, 1, 0.5f);
		}

		void ClosePanel(Transform tr)
		{
			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayClosePanel ();
			
			m_blurPanel.ClosePanel (() => IsOpen = false);
			JKTween.ScaleTransform (transform, Vector3.one, 0, 0.5f, () => 
			{
				m_screen.RestartGame(m_timerIsPaused);
			});
		}
	}
}