using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using JK.Effects;
using UnityEngine.UI;

namespace Xenubi
{
	public class GameplayDeck : MonoBehaviour 
	{
		#region Delegates
		
		public delegate void MethodClickedElement(ElementType elementType);
		private MethodClickedElement MethodElementClickedCallback;
		
		#endregion

		#region Fields

		private GameplayCard[] m_cards;

		[SerializeField] private Color replacedColor;

		#endregion

		#region Methods

		public void Setup(Element[] elements, MethodClickedElement callback = null)
		{
			if (m_cards == null)
			{
				Initialize ();
			}

			MethodElementClickedCallback = callback;

			FillCards (elements);
			GameplayService.PlayCardPlace ();

			float originalPositionX = callback != null ? -Screen.width : Screen.width;
			JKTween.LocalMoveXTransform (transform, originalPositionX * 2, transform.localPosition.x, 1f);
		}

		public void ShowCurrentCard(int currentMatch)
		{
			m_cards [currentMatch].enabled = true;
		}

		public void HideAllCards()
		{
			for (int i = 0; i < m_cards.Length; i++) 
			{
				m_cards [i].Hide ();
			}
		}

		public void HideCurrentCard(int currentMatch)
		{
			m_cards [currentMatch].Hide ();
		}

		public void MirrorDeck(int currentMatch, int index, Action callback)
		{
			m_cards [currentMatch].MirrorCard (index, replacedColor, callback);
		}

		void Initialize()
		{
			m_cards = GetComponentsInChildren<GameplayCard>().OrderBy (c => c.name).ToArray();
		}
		
		void FillCards(Element[] elements)
		{
			for (int i = 0; i < m_cards.Length; i++)
			{
				m_cards[i].Setup(EvaluateClickedElementCallback, elements[i]);
			}
		}

		void EvaluateClickedElementCallback(ElementType elementType, Image image)
		{
			image.color = replacedColor;
			image.sprite = null;

			JKTween.AlphaColorImage(image, 1, true, 0.5f, () => 
			{
				MethodElementClickedCallback (elementType);
			});
		}

		#endregion
	}
}