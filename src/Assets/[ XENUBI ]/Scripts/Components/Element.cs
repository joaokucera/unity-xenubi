﻿using System.Collections;
using UnityEngine;

namespace Xenubi
{
	public class Element 
	{
		public int Row;
		public int Column;
		public string Name;
		public string Symbol;
		public int AtomicNumber;
		public float AtomicMass;
		public float AtomicRadius;
		public float ElectronNegativity;
		public float IonizationEnergy;
		public float ElectronAffinity;
		public float Density;
		public string PF;
		public float MeltingPoint;
		public string BoilingPoint;
		public string Abundance;

		public string AtomicNumberToShow { get { return AtomicNumber.ToString(); } }
		public string AtomicMassToShow { get { return AtomicMass.ToString(); } }
		public string AtomicRadiusToShow { get { return string.Format("{0} pm", AtomicRadius); } }
		public string IonizationEnergyToShow { get { return string.Format("{0} kJ/mol", IonizationEnergy); } }
		public string ElectronAffinityToShow { get { return string.Format("{0} kJ/mol", ElectronAffinity); } }
		public string ElectronNegativityToShow { get { return ElectronNegativity.ToString(); } }
		public string MeltingPointToShow { get { return string.Format("{0} ºC", MeltingPoint); } }
		public string DensityToShow { get { return string.Format("{0} g/cm³", Density); } }

		public override string ToString()
		{
			return string.Format("Name: {0} - Symbol: {1} - Row: {2} - Column: {3}", Name, Symbol, Row, Column);
		}
	}
}