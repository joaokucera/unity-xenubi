using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;
using Xenubi.Screens;

namespace Xenubi
{
	public class PanelQuit : MonoBehaviour
	{
		private Button m_yesButton;
		private Button m_noButton;

		private PanelBlur m_blurPanel;
		private GenericScreen m_screen;
		private ScreenName m_screenNameOption = ScreenName.Quit;

		[SerializeField] private Text m_text;

		public bool IsOpen { get; private set; }

		void Start()
		{
			m_screen = FindObjectOfType<GenericScreen> ();
			m_blurPanel = FindObjectOfType<PanelBlur> ();

			m_text.text = string.Empty;

			PrepareButtons ();
		}

		void PrepareButtons()
		{
			Button[] buttons = GetComponentsInChildren<Button>();
			
			for (int i = 0; i < buttons.Length; i++)
			{
				if (buttons[i].name.Contains(ButtonName.Yes.ToString()))
				{
					m_yesButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => 
					{
						if (m_screenNameOption == ScreenName.Quit)
						{
							Application.Quit();
						}
						else
						{
							m_screen.OnAction(m_screenNameOption, m_yesButton.transform);
						}
					});
				}
				else if (buttons[i].name.Contains(ButtonName.No.ToString()))
				{
					m_noButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => ClosePanel(m_noButton.transform));
				}
			}
		}

		public void OpenPanel(ScreenName screenName, string message, Transform tr = null)
		{
			IsOpen = true;

			if (tr != null)
			{
				JKTween.ShakeScaleTransform (tr);
			}

			GameplayService.PlayOpenPanel ();
			JKTween.ScaleTransform (transform, Vector3.zero, 1, 0.5f);

			m_screenNameOption = screenName;
			m_text.text = message;

			if (m_screenNameOption == ScreenName.Quit)
			{
				m_blurPanel.OpenPanel ();
			}
		}

		void ClosePanel(Transform tr)
		{
			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayClosePanel ();

			JKTween.ScaleTransform (transform, Vector3.one, 0, 0.5f);

			if (m_screenNameOption == ScreenName.Quit)
			{
				m_blurPanel.ClosePanel (() => IsOpen = false);

				(m_screen as MenuScreen).EnableGraphicRaycaster();
			}
			else
			{
				IsOpen = false;
			}
		}
	}
}