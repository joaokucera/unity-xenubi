﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;
using System;

namespace Xenubi
{
	public class TutorialEffects_3 : MonoBehaviour, ITutorialEffects
	{
		private float duration;
		private int m_currentIndex;

		[SerializeField] private RectTransform[] rects;

		public void Initialize ()
		{
		}

		public void Show(Action callback)
		{
			duration = 0.5f;

			m_currentIndex = 0;

			TryNext (callback);
		}

		public void TryNext(Action callback)
		{
			JKTween.PunchTransform(rects[m_currentIndex], Vector3.one / 4, duration, () => 
			{
				m_currentIndex++;

				if (m_currentIndex < rects.Length)
				{
					TryNext(callback);
				}
				else
				{
					callback();
				}
			});
		}

		public void Finish()
		{
			duration /= 2;
		}
	}
}