﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using JK.Patterns;
using JK.Effects;

namespace Xenubi
{
	public class GameplayPeriodicTable : MonoBehaviour
	{
		#region [ FIELDS ]

		private bool m_alreadyHide;
		private GameplayPeriodicElement[] m_elements;

		[SerializeField] private Sprite defaultSprite;

		#endregion

		#region Methods (Public)

		public void Setup()
		{
			if (m_elements == null)
			{
				Initialize();
			}

			JKTween.LocalMoveYTransform (transform, -Screen.height * 2, transform.localPosition.y, 1f);
		}

		void Initialize()
		{
			m_elements = GetComponentsInChildren<GameplayPeriodicElement>();
		}

		public void Hide(Action callback)
		{
			if (!m_alreadyHide)
			{
				m_alreadyHide = true;

				var value = 4f;
				JKTween.LocalMoveYTransform (transform, transform.localPosition.y, -Screen.height * value, 1f * value / 2, callback);
			}
			else
			{
				callback();
			}
		}

		public void ClearData(int row, int column)
		{
			GameplayPeriodicElement singleElement = m_elements.FirstOrDefault(e => e.Row == row && e.Column == column);
			
			if (singleElement != null)
			{
				singleElement.Clear(defaultSprite);
			}
		}

		public void SetData(Sprite sprite, int row, int column)
		{
			GameplayPeriodicElement singleElement = m_elements.FirstOrDefault(e => e.Row == row && e.Column == column);

			if (singleElement != null)
			{
				singleElement.SetData(sprite);
			}
		}

		#endregion
	}
}