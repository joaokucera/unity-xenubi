﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;

namespace Xenubi
{
	public class GameplayPeriodicElement : MonoBehaviour
	{
		#region Variables

		private Image m_image;
		public Image Image
		{
			get
			{
				if (m_image == null)
				{
					m_image = GetComponent<Image>();
				}
				
				return m_image;
			}
		}

		private int m_row;
		public int Row
		{
			get
			{
				if (m_row == 0)
				{
					string rowString = transform.parent.name.Replace("Row ", "");

					m_row = Convert.ToInt32(rowString);
				}

				return m_row;
			}
		}

		private int m_column;
		public int Column
		{
			get
			{
				if (m_column == 0)
				{
					string columnString = transform.name.Replace("Column ", "");
					
					m_column = Convert.ToInt32(columnString);
				}
				
				return m_column;
			}
		}

		#endregion

		#region Methods

		public void SetData(Sprite sprite)
		{
			Image.sprite = sprite;
		}

		public void Clear(Sprite defaultSprite)
		{
			Image.sprite = defaultSprite;
		}

		#endregion
	}
}