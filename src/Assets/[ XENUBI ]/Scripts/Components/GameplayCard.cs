using System;
using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;
using JK.Patterns;
using Random = UnityEngine.Random;

namespace Xenubi
{
	public class GameplayCard : JKSingleton<GameplayCard>
	{
		#region Delegates
		
		public delegate void MethodClickedElement(ElementType elementType, Image image);
		private MethodClickedElement MethodElementClickedCallback;
		
		#endregion

		#region Fields

		private GameplayManager m_manager;
		private Element m_element;

		private bool m_alreadyHide;
		private bool m_alreadyClicked;

		private Button m_atomicRadiusButton;
		private Button m_ionizationEnergyButton;
		private Button m_electronAffinityButton;
		private Button m_electronNegativityButton;
		private Button m_meltingPointButton;
		private Button m_densityButton;

		private Transform[] m_valuesTransform;

		public bool IsPlayer { get { return transform.parent.name.Contains ("Player"); } }

		#endregion

		#region Methods (Public)

		public void Setup (MethodClickedElement callback, Element element)
		{
			MethodElementClickedCallback = callback;

			m_manager = FindObjectOfType<GameplayManager> ();
			PrepareButtons ();

			m_element = element;
			FillData ();
		
			enabled = false;
		}

		public void MirrorCard (int index, Color replacedColor, Action callback)
		{
			GameplayService.PlayClickButton ();

			JKTween.ScaleTransform (m_valuesTransform[index], m_valuesTransform[index].localScale, 1.25f, 1f, () =>
			{
				FillValues();

				Image image = m_valuesTransform[index].GetComponent<Image>();
				image.color = replacedColor;
				image.sprite = null;

				JKTween.AlphaColorImage (image, 1, true, 0.5f, callback);
			});
		}

		public void Hide()
		{
			if (!m_alreadyHide)
			{
				m_alreadyHide = true;

				var value = 4f;
				JKTween.LocalMoveYTransform (transform, transform.localPosition.y, -Screen.height * value, 1f * value / 2);

				GameplayService.PlayCardPlace ();
			}
		}

		#endregion

		#region Methods (Private)

		void FillData()
		{
			RoundChallenge challenge = m_manager.Round.GetCurrentRoundChallenge ();
			int challengeIndex = (int)challenge;

			FillSymbol ();

			if (IsPlayer || challengeIndex >= (int)RoundChallenge.OnlySymbolAndName)
			{
				FillName ();
			}

			if (IsPlayer || challengeIndex >= (int)RoundChallenge.NoProperties)
			{
				FillHead ();
			}

			var valuesGroup = transform.FindChild ("Content/Values Group");
			{
				m_valuesTransform = new Transform[6];
				
				m_valuesTransform[0] = valuesGroup.FindChild ("AtomicRadius Button");
				m_valuesTransform[1] = valuesGroup.FindChild ("IonizationEnergy Button");
				m_valuesTransform[2] = valuesGroup.FindChild ("ElectronAffinity Button");
				m_valuesTransform[3] = valuesGroup.FindChild ("ElectronNegativity Button");
				m_valuesTransform[4] = valuesGroup.FindChild ("MeltingPoint Button");
				m_valuesTransform[5] = valuesGroup.FindChild ("Density Button");

				FillElements();

				if (IsPlayer || challengeIndex >= (int)RoundChallenge.WholeCard)
				{
					FillValues();
				}
				else if (challengeIndex == (int)RoundChallenge.RandomProperties)
				{
					FillValues (true);
				}
			}
		}

		void FillHead ()
		{
			var headGroup = transform.FindChild ("Content/Head Group");
			{
				Text atomicNumberText = headGroup.FindChild ("AtomicNumber Text").GetComponent<Text> ();
				atomicNumberText.text = m_element.AtomicNumberToShow;
				Text atomicMassText = headGroup.FindChild ("AtomicMass Text").GetComponent<Text> ();
				atomicMassText.text = m_element.AtomicMassToShow;
			}
		}

		void FillSymbol ()
		{
			var headGroup = transform.FindChild ("Content/Head Group");
			{
				Text symbolText = headGroup.FindChild ("Symbol Text").GetComponent<Text> ();
				symbolText.text = m_element.Symbol;
			}
		}

		void FillName ()
		{
			var content = transform.FindChild ("Content");
			{
				Text nameText = content.FindChild ("Name Text").GetComponent<Text> ();
				nameText.text = m_element.Name;
			}
		}

		void FillElements()
		{
			var translation = Translator.CurrentTranslation;
			
			Text atomicRadiusName = m_valuesTransform [0].FindChild ("Name Text").GetComponent<Text> ();
			{
				atomicRadiusName.text = translation.Game.PeriodicTable.AtomicRadius;
			}
			Text ionizationEnergyName = m_valuesTransform [1].FindChild ("Name Text").GetComponent<Text> ();
			{
				ionizationEnergyName.text = translation.Game.PeriodicTable.IonizationEnergy;
			}
			Text electronAffinityName = m_valuesTransform [2].FindChild ("Name Text").GetComponent<Text> ();
			{
				electronAffinityName.text = translation.Game.PeriodicTable.ElectronAffinity;
			}
			Text electronNegativityName = m_valuesTransform [3].FindChild ("Name Text").GetComponent<Text> ();
			{
				electronNegativityName.text = translation.Game.PeriodicTable.ElectronNegativity;
			}
			Text meltingPoinName = m_valuesTransform [4].FindChild ("Name Text").GetComponent<Text> ();
			{
				meltingPoinName.text = translation.Game.PeriodicTable.MeltingPoint;
			}
			Text densityName = m_valuesTransform [5].FindChild ("Name Text").GetComponent<Text> ();
			{
				densityName.text = translation.Game.PeriodicTable.Density;
			}
		}

		void FillValues (bool randomValues = false)
		{
			Text atomicRadiusText = m_valuesTransform [0].FindChild ("Value Text").GetComponent<Text> ();
			Text ionizationEnergyText = m_valuesTransform [1].FindChild ("Value Text").GetComponent<Text> ();
			Text electronAffinityText = m_valuesTransform [2].FindChild ("Value Text").GetComponent<Text> ();
			Text electronNegativityText = m_valuesTransform [3].FindChild ("Value Text").GetComponent<Text> ();
			Text meltingPointText = m_valuesTransform [4].FindChild ("Value Text").GetComponent<Text> ();
			Text densityText = m_valuesTransform [5].FindChild ("Value Text").GetComponent<Text> ();

			if (randomValues)
			{
				var length = m_valuesTransform.Length / 2;
				for (int i = 0; i < length; i++) 
				{
					int rnd = Random.Range(0, m_valuesTransform.Length);

					if (rnd == 0) atomicRadiusText.text = m_element.AtomicRadiusToShow;
					else if (rnd == 1) ionizationEnergyText.text = m_element.IonizationEnergyToShow;
					else if (rnd == 2) electronAffinityText.text = m_element.ElectronAffinityToShow;
					else if (rnd == 3) electronNegativityText.text = m_element.ElectronNegativityToShow;
					else if (rnd == 4) meltingPointText.text = m_element.MeltingPointToShow;
					else if (rnd == 5) densityText.text = m_element.DensityToShow;
				}
			}
			else
			{
				atomicRadiusText.text = m_element.AtomicRadiusToShow;
				ionizationEnergyText.text = m_element.IonizationEnergyToShow;
				electronAffinityText.text = m_element.ElectronAffinityToShow;
				electronNegativityText.text = m_element.ElectronNegativityToShow;
				meltingPointText.text = m_element.MeltingPointToShow;
				densityText.text = m_element.DensityToShow;
			}
		}

		void PrepareButtons ()
		{
			Button[] buttons = GetComponentsInChildren<Button>();

			for (int i = 0; i < buttons.Length; i++)
			{
				if (buttons[i].name.Contains(ElementType.AtomicRadius.ToString()))
				{
					m_atomicRadiusButton = buttons[i];

					buttons[i].onClick.AddListener(() => OnClickElement(ElementType.AtomicRadius, m_atomicRadiusButton.transform));
				}
				else if (buttons[i].name.Contains(ElementType.IonizationEnergy.ToString()))
				{
					m_ionizationEnergyButton = buttons[i];

					buttons[i].onClick.AddListener(() => OnClickElement(ElementType.IonizationEnergy, m_ionizationEnergyButton.transform));
				}
				else if (buttons[i].name.Contains(ElementType.ElectronAffinity.ToString()))
				{
					m_electronAffinityButton = buttons[i];

					buttons[i].onClick.AddListener(() => OnClickElement(ElementType.ElectronAffinity, m_electronAffinityButton.transform));
				}
				else if (buttons[i].name.Contains(ElementType.ElectronNegativity.ToString()))
				{
					m_electronNegativityButton = buttons[i];

					buttons[i].onClick.AddListener(() => OnClickElement(ElementType.ElectronNegativity, m_electronNegativityButton.transform));
				}
				else if (buttons[i].name.Contains(ElementType.MeltingPoint.ToString()))
				{
					m_meltingPointButton = buttons[i];

					buttons[i].onClick.AddListener(() => OnClickElement(ElementType.MeltingPoint, m_meltingPointButton.transform));
				}
				else if (buttons[i].name.Contains(ElementType.Density.ToString()))
				{
					m_densityButton = buttons[i];

					buttons[i].onClick.AddListener(() => OnClickElement(ElementType.Density, m_densityButton.transform));
				}
			}
		}
		
		void OnClickElement(ElementType elementType, Transform tr)
		{
			if (!m_alreadyClicked)
			{
				m_alreadyClicked = true;
				m_manager.OnPauseTimer();

				GameplayService.PlayClickButton ();

				JKTween.ScaleTransform (tr, tr.localScale, 1.25f, 1f, () => 
				{
					MethodElementClickedCallback (elementType, tr.GetComponent<Image>());
				});
			}
		}

		#endregion
	}
}