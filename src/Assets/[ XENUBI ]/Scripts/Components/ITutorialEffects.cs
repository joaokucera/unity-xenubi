﻿using System;
using System.Collections;
using UnityEngine;

namespace Xenubi
{
	public interface ITutorialEffects
	{
		void Initialize();

		void Show(Action callback);

		void TryNext(Action callback);

		void Finish();
	}
}