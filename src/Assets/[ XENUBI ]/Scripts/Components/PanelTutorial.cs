using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;
using Xenubi.Screens;
using JK.Managment;

namespace Xenubi
{
	public class PanelTutorial : MonoBehaviour
	{
		private Button m_yesButton;
		private Button m_noButton;

		private GameScreen m_screen;
		private GameplayTutorial m_tutorial;
		private PanelBlur m_blurPanel;

		void Start()
		{
			m_screen = FindObjectOfType<GameScreen> ();

			if (JKManagment.TutorialShowOff)
			{
				m_screen.StartGame();
			}
			else
			{
				JKManagment.TutorialShowOff = true;

				m_tutorial = FindObjectOfType<GameplayTutorial> ();
				m_blurPanel = FindObjectOfType<PanelBlur> ();

				PrepareButtons ();

				StartCoroutine(OpenPanel ());
			}
		}
		
		void PrepareButtons()
		{
			Button[] buttons = GetComponentsInChildren<Button>();
			
			for (int i = 0; i < buttons.Length; i++)
			{
				if (buttons[i].name.Contains(ButtonName.Yes.ToString()))
				{
					m_yesButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => ClosePanel(m_yesButton.transform, false, m_tutorial.Show));
				}
				else if (buttons[i].name.Contains(ButtonName.No.ToString()))
				{
					m_noButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => ClosePanel(m_noButton.transform, true, m_screen.StartGame));
				}
			}
		}
		
		IEnumerator OpenPanel()
		{
			var delay = JKManagment.IntroShowOff ? 0 : 2f;
			yield return new WaitForSeconds (delay);

			GameplayService.PlayOpenPanel ();

			m_blurPanel.OpenPanel ();
			JKTween.ScaleTransform (transform, Vector3.zero, 1, 0.5f);
		}
		
		void ClosePanel(Transform tr, bool toCloseBlurPanel, Action callback = null)
		{
			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayClosePanel ();

			if (toCloseBlurPanel)
			{
				m_blurPanel.ClosePanel ();
			}
			JKTween.ScaleTransform (transform, Vector3.one, 0, 0.5f, callback);
		}
	}
}