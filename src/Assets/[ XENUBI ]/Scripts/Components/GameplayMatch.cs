﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Patterns;
using System;
using JK.Effects;

namespace Xenubi
{
	public class GameplayMatch : MonoBehaviour
	{
		#region Fields

		private bool m_alreadyHide;
		private Image m_feedbackImage;

		#endregion

		#region Properties

		[HideInInspector] public bool IsPlayerVictory;
		[HideInInspector] public ElementType ElementTypeSelected;
		
		#endregion

		#region Methods (Public)
		
		public void SetMatchResult(bool result, ElementType elementType, Sprite other, Action callback)
		{
			if (m_feedbackImage == null)
			{
				Initialize();
			}

			IsPlayerVictory = result;
			ElementTypeSelected = elementType;

			m_feedbackImage.sprite = other;	
			JKTween.AlphaColorImage (m_feedbackImage, 1, true, 0.5f);

			JKTween.PunchTransform (m_feedbackImage.transform, Vector3.one * 2, 1f, callback);
		}

		public void Show()
		{
			JKTween.LocalMoveYTransform (transform.parent, Screen.height * 2, transform.parent.localPosition.y, 1f);
		}

		public void Hide()
		{
			if (!m_alreadyHide)
			{
				m_alreadyHide = true;

				var value = 5f;
				JKTween.LocalMoveYTransform (transform.parent, transform.parent.localPosition.y, Screen.height * value, 1f * value / 2);
			}
		}

		#endregion

		#region Methods (Private)

		void Initialize()
		{
			m_feedbackImage = transform.FindChild("Feedback Image").GetComponent<Image> ();

			m_feedbackImage.enabled = false;
			m_feedbackImage.sprite = null;

			Color color = m_feedbackImage.color;
			color.a = 0;
			m_feedbackImage.color = color;
		}

		#endregion
	}
}