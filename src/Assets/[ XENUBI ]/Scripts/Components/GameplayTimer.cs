﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;
using JK.Patterns;

namespace Xenubi
{
	public class GameplayTimer : MonoBehaviour
	{
		public delegate void MethodOvertime();
		private MethodOvertime MethodOvertimeCallback;

		public delegate void MethodLowTime();
		private MethodLowTime MethodLowTimeCallback;

		private const float MaxTimeInSeconds = 120f;
		private const float MinTimeInSeconds = 20f;
		private const int MinTimeToWarning = 10;

		private float m_currentTimeInSeconds;
		private bool m_isLowerThanMinTime;
		private Text m_timerNumberText;
		private bool m_isPaused;

		[SerializeField] private Color defaultColor;
		[SerializeField] private Color warningColor;

		public bool IsPaused { get { return m_isPaused; } }

		void Start()
		{
			Initialize ();
		}

		public void Setup (MethodOvertime overtimeCallback, MethodLowTime lowTimeCallback)
		{
			MethodOvertimeCallback = overtimeCallback;
			MethodLowTimeCallback = lowTimeCallback;

			if (m_timerNumberText == null)
			{
				Initialize();
			}

			m_currentTimeInSeconds = MaxTimeInSeconds;
		}

		public void OnStart()
		{
			m_isPaused = false;

			m_isLowerThanMinTime = false;
			m_timerNumberText.color = defaultColor;

			StartCoroutine ("UpdateTimer", m_currentTimeInSeconds);
		}

		public void OnRestart()
		{
			m_isPaused = false;
		}

		public void OnPause()
		{
			m_isPaused = true;
		}

		public void OnStop()
		{
			StopCoroutine ("UpdateTimer");

			GenerateNextTimer ();
		}

		void Initialize()
		{
			m_timerNumberText = transform.FindChild ("Timer Number").GetComponentInChildren<Text> ();
		}

		void GenerateNextTimer()
		{
			m_currentTimeInSeconds -= 5f;

			m_currentTimeInSeconds = Mathf.Clamp (m_currentTimeInSeconds, MinTimeInSeconds, MaxTimeInSeconds);
		}

		IEnumerator UpdateTimer(float timeInSeconds)
		{
			List<int> warningTimes = GetWarningTimes ();

			UpdateTimerText(timeInSeconds);
			yield return new WaitForSeconds(1f);

			while (timeInSeconds > 0)
			{
				if (!m_isPaused)
				{
					timeInSeconds -= Time.deltaTime;

					if (!m_isLowerThanMinTime && timeInSeconds < MinTimeToWarning)
					{
						m_isLowerThanMinTime = true;
						m_timerNumberText.color = warningColor;

						JKTween.PunchTransform (m_timerNumberText.transform, Vector3.one / 2, 1f);

						MethodLowTimeCallback();
					}

					if (m_isLowerThanMinTime && warningTimes.Contains((int)timeInSeconds))
					{
						warningTimes.Remove((int)timeInSeconds);
						GameplayService.PlayCustom("Click1");
					}

					UpdateTimerText(timeInSeconds);
				}
				
				yield return null;
			}

			UpdateTimerText(0);
			MethodOvertimeCallback ();
		}

		void UpdateTimerText(float timeInSeconds)
		{
			TimeSpan interval = TimeSpan.FromSeconds (timeInSeconds);

			m_timerNumberText.text = string.Format("{0:00}:{1:00}", interval.Minutes, interval.Seconds);
		}

		List<int> GetWarningTimes()
		{
			var warningTimes = new List<int> ();

			for (int i = 0; i < MinTimeToWarning; i++)
			{
				warningTimes.Add(i);
			}

			return warningTimes;
		}
	}
}