using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;
using JK.Integrations;
using JK.Managment;
using System.Collections.Generic;

namespace Xenubi.Screens
{
	public class GameScreen : GenericScreen 
	{
		#region Fields

		private bool m_pauseButtonVisible;
		private Button m_pauseButton;
		private Vector3 m_pauseButtonInside;

		private GameplayManager m_manager;
		private PanelPause m_pausePanel;

		[SerializeField] protected AudioClip warningClip;

		#endregion

		#region Events

		protected override void Start()
		{
			base.Start ();

			m_pausePanel = FindObjectOfType<PanelPause> ();
			m_pausePanel.Setup ();
			
			m_manager = FindObjectOfType<GameplayManager> ();
			m_manager.Setup();

			SetupFacebookProfile (FB.IsLoggedIn);
		}

		public void SetupFacebookProfile(bool isLogged)
		{
			if (isLogged)
			{
				JKFacebookProfile.SetProfileInGame ();
				JKFacebookProfile.SetProfilePicture ();
			}
		}

		#endregion

		#region implemented abstract members of GenericScreen

		protected override void BackButtonClick ()
		{
			if (m_pauseButtonVisible && !m_pausePanel.IsOpen && Input.GetKeyDown (KeyCode.Escape))
			{
				PauseButtonClick();
			}
		}

		protected override void PrepareButtons ()
		{
			m_pauseButton = GameObject.Find("Pause Button").GetComponent<Button>();

			m_pauseButton.onClick.AddListener(() => 
			{
				PauseButtonClick();
			});

			m_pauseButtonInside = m_pauseButton.transform.localPosition;
			m_pauseButton.transform.localPosition = new Vector3(Screen.width * 2, m_pauseButton.transform.localPosition.y, m_pauseButton.transform.localPosition.z);
		}

		protected override void PrepareTexts ()
		{
			var translation = Translator.CurrentTranslation;
			var playerElement = Translator.CurrentElements[0];
			var enemyElement = Translator.CurrentElements[1];

			var dic = new Dictionary<string, string>()
			{
				// READY
				{ "@ReadyMessage", translation.Game.ReadyMessage },
				// HEADER
				{ "@DefaultScore", string.Format("0 {0}s", translation.Game.ResultPanel.Score) },
				{ "@HeaderRound", string.Format("1ª {0}", translation.Game.Header.Round) },
				{ "@HeaderTimer", translation.Game.Header.Timer },
				// ELEMENTS
				{ "@Player Name Text", playerElement.Name },
				{ "@Enemy Name Text", enemyElement.Name },
				{ "@AtomicRadius Name Text", translation.Game.PeriodicTable.AtomicRadius },
				{ "@IonizationEnergy Name Text", translation.Game.PeriodicTable.IonizationEnergy },
				{ "@ElectronAffinity Name Text", translation.Game.PeriodicTable.ElectronAffinity },
				{ "@ElectronNegativity Name Text", translation.Game.PeriodicTable.ElectronNegativity },
				{ "@MeltingPoint Name Text", translation.Game.PeriodicTable.MeltingPoint },
				{ "@Density Name Text", translation.Game.PeriodicTable.Density },
				// PAUSE PANEL
				{ "@PausePanelTitle", translation.Game.PausePanel.Title },
				{ "@PausePanelTip", translation.Game.PausePanel.Tip },
				// TIP PANEL
				{ "@TipPanelTitle", translation.Game.TipPanel.Title },
				{ "@TipPanelAtomicRadiusTitle", translation.Game.PeriodicTable.AtomicRadius },
				{ "@TipPanelAtomicRadiusSideMessage", translation.Game.TipPanel.Content.AtomicRadius.SideMessage },
				{ "@TipPanelAtomicRadiusBottomMessage", translation.Game.TipPanel.Content.AtomicRadius.BottomMessage },
				{ "@TipPanelIonizationEnergyTitle", translation.Game.PeriodicTable.IonizationEnergy },
				{ "@TipPanelIonizationEnergySideMessage", translation.Game.TipPanel.Content.IonizationEnergy.SideMessage },
				{ "@TipPanelIonizationEnergyBottomMessage", translation.Game.TipPanel.Content.IonizationEnergy.BottomMessage },
				{ "@TipPanelElectronAffinityTitle", translation.Game.PeriodicTable.ElectronAffinity },
				{ "@TipPanelElectronAffinitySideMessage", translation.Game.TipPanel.Content.ElectronAffinity.SideMessage },
				{ "@TipPanelElectronAffinityBottomMessage", translation.Game.TipPanel.Content.ElectronAffinity.BottomMessage },
				{ "@TipPanelElectronNegativityTitle", translation.Game.PeriodicTable.ElectronNegativity },
				{ "@TipPanelElectronNegativitySideMessage", translation.Game.TipPanel.Content.ElectronNegativity.SideMessage },
				{ "@TipPanelElectronNegativityBottomMessage", translation.Game.TipPanel.Content.ElectronNegativity.BottomMessage },
				{ "@TipPanelMeltingPointTitle", translation.Game.PeriodicTable.MeltingPoint },
				{ "@TipPanelMeltingPointSideMessage", translation.Game.TipPanel.Content.MeltingPoint.SideMessage },
				{ "@TipPanelMeltingPointBottomMessage", translation.Game.TipPanel.Content.MeltingPoint.BottomMessage },
				{ "@TipPanelDensityTitle", translation.Game.PeriodicTable.Density },
				{ "@TipPanelDensitySideMessage", translation.Game.TipPanel.Content.Density.SideMessage },
				{ "@TipPanelDensityBottomMessage", translation.Game.TipPanel.Content.Density.BottomMessage },
				// TUTORIAL PANEL
				{ "@TutorialPanelTitle", translation.Game.TutorialPanel.Title },
				{ "@TutorialPanelQuestion", translation.Game.TutorialPanel.Question },
				{ "@TutorialPanelNo", translation.Game.TutorialPanel.Buttons.No },
				{ "@TutorialPanelYes", translation.Game.TutorialPanel.Buttons.Yes },
				{ "@TutorialPanelSkip", translation.Game.TutorialPanel.Buttons.Skip },
				{ "@TutorialPanelRoundMatch", translation.Game.TutorialPanel.Content.RoundMatch },
				{ "@TutorialPanelPeriodicTable", translation.Game.TutorialPanel.Content.PeriodicTable },
				{ "@TutorialPanelDeckPlayer", translation.Game.TutorialPanel.Content.DeckPlayer },
				{ "@TutorialPanelDeckEnemy", translation.Game.TutorialPanel.Content.DeckEnemy },
				// MATCH PANEL
				{ "@MatchPanelTitle", string.Format("{0}/{1}", translation.Game.MatchPanel.Victory, translation.Game.MatchPanel.Defeat) },
				{ "@MatchPanelAtomicRadius", string.Format("{0}: <color=#EEBB12>{1}</color>", translation.Game.MatchPanel.Choice, translation.Game.PeriodicTable.AtomicRadius) },
				{ "@MatchPanelIonizationEnergy", string.Format("{0}: <color=#EEBB12>{1}</color>", translation.Game.MatchPanel.Choice, translation.Game.PeriodicTable.IonizationEnergy) },
				{ "@MatchPanelElectronAffinity", string.Format("{0}: <color=#EEBB12>{1}</color>", translation.Game.MatchPanel.Choice, translation.Game.PeriodicTable.ElectronAffinity) },
				{ "@MatchPanelElectronNegativity", string.Format("{0}: <color=#EEBB12>{1}</color>", translation.Game.MatchPanel.Choice, translation.Game.PeriodicTable.ElectronNegativity) },
				{ "@MatchPanelMeltingPoint", string.Format("{0}: <color=#EEBB12>{1}</color>", translation.Game.MatchPanel.Choice, translation.Game.PeriodicTable.MeltingPoint) },
				{ "@MatchPanelDensity", string.Format("{0}: <color=#EEBB12>{1}</color>", translation.Game.MatchPanel.Choice, translation.Game.PeriodicTable.Density) },
				{ "@MatchPanelToggle", translation.Game.MatchPanel.Toggle },
				// INFO PANEL
				{ "@InfoPanelTitle", translation.Game.InfoPanel.Title },
				// RESULT PANEL
				{ "@ResultPanelTitle", translation.Game.ResultPanel.Title },
				// QUIT PANEL
				{ "@QuitPanelTitle", translation.QuitPanel.Title },
				{ "@QuitPanelNo", translation.QuitPanel.Buttons.No },
				{ "@QuitPanelYes", translation.QuitPanel.Buttons.Yes },
				// FACEBOOK
				{ "@FacebookGuest", translation.Facebook.Guest },
				{ "@FacebookConnect", translation.Facebook.Connect },
				{ "@FacebookShare", translation.Facebook.Share }
			};
			
			Text[] texts = FindObjectsOfType<Text>();
			
			foreach (var item in texts) 
			{
				string message;
				if (dic.TryGetValue(item.name, out message))
				{
					item.text = message;
				}
			}
		}

		#endregion

		#region Methods

		public void StartGame()
		{
			m_manager.StartGameplay ();
		}

		public void ShowPauseButton()
		{
			JKTween.LocalMoveXTransform (m_pauseButton.transform, m_pauseButton.transform.localPosition.x, m_pauseButtonInside.x, 1f, () =>
            {
				m_pauseButtonVisible = true;
			});
		}

		public void RestartGame (bool timerIsPaused)
		{
			JKTween.Pause = false;

			if (!timerIsPaused)
			{
				m_manager.OnRestartTimer();
			}
		}

		public void PlayWarningMusic()
		{
			JKSoundManager.PlayMusicImmediate (warningClip);
		}

		void PauseButtonClick()
		{
			JKTween.Pause = true;
			
			m_pausePanel.OpenPanel(m_pauseButton.transform, m_manager.IsAlreadyPaused());
			
			m_manager.OnPauseTimer();
		}

		#endregion
	}
}