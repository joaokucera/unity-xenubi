﻿using System.Collections;
using UnityEngine;
using JK.Effects;
using JK.Integrations;
using UnityEngine.UI;

namespace Xenubi.Screens
{
	public abstract class GenericScreen : MonoBehaviour 
	{
		#region Fields

		protected CanvasGroup m_canvasGroup;
		protected GraphicRaycaster m_graphicRaycaster;

		[SerializeField] protected AudioClip musicClip;

		#endregion

		#region Events

		protected virtual void Awake()
		{
			m_canvasGroup = GameObject.Find ("Canvas").GetComponent<CanvasGroup> ();
			m_graphicRaycaster = m_canvasGroup.GetComponent<GraphicRaycaster> ();

			JKTween.ShowCanvasGroup (m_canvasGroup, 2f, () =>
			{
				JKUnityAnalyticsService.SendSceneLoad (Application.loadedLevelName);
			});
		}

		protected virtual void Start()
		{
			PrepareMusic ();
			
			PrepareTexts ();

			PrepareButtons ();
		}
		
		void Update()
		{
			BackButtonClick ();
		}

		#endregion

		#region Methods

		protected abstract void BackButtonClick();

		protected abstract void PrepareButtons();

		protected abstract void PrepareTexts();

		protected virtual void PrepareMusic()
		{
			JKSoundManager.PlayMusic (musicClip);
		}

		public void PlayDefaultMusicImmediate()
		{
			JKSoundManager.PlayMusicImmediate (musicClip);
		}

		public void OnAction(ScreenName screenName, Transform tr = null)
		{
			if (tr != null)
			{
				JKTween.ShakeScaleTransform (tr);

				GameplayService.PlayClickButton ();
			}

			StartCoroutine(JKTween.HideCanvasGroup (m_canvasGroup, 2f, (int)screenName));
		}

		#endregion
	}
}