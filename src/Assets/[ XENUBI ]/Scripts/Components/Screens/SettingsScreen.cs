using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;
using System.Collections.Generic;
using System.Text;

namespace Xenubi.Screens
{
	public class SettingsScreen : GenericScreen 
	{
		private Button m_musicButton;
		private Button m_soundEffectButton;
		private Button m_howToPlayButton;
		private Button m_creditsButton;
		private Button m_backButton;
		private Button m_languageButton;

		private Button m_howToPlayCloseButton;
		private Button m_creditsCloseButton;
		
		private bool m_enableLanguageButton = true;

		#region Variables (Inspector)

		[SerializeField] private Image blurPainelImage;
		[SerializeField] private Transform howToPlayPanelTransform;
		[SerializeField] private Transform creditsPanelTransform;
		
		[SerializeField] private AudioSettings audioSettings;
		[SerializeField] private Image flagImage;
		[SerializeField] private Sprite[] flags;

		#endregion

		#region Events

		protected override void Start()
		{
			base.Start ();

			blurPainelImage.enabled = false;
			blurPainelImage.color = new Color (1, 1, 1, 0);

			howToPlayPanelTransform.localScale = Vector3.zero;
			creditsPanelTransform.localScale = Vector3.zero;

			audioSettings.PrepareIcons ();

			flagImage.sprite = flags [Translator.CurrentLanguage];
		}

		#endregion

		#region implemented abstract members of GenericScreen

		protected override void BackButtonClick ()
		{
			if (Input.GetKeyDown (KeyCode.Escape))
			{
				m_graphicRaycaster.enabled = false;

				OnAction(ScreenName.Menu, m_backButton.transform);
			}
		}

		protected override void PrepareButtons ()
		{
			Button[] buttons = GameObject.FindObjectsOfType<Button>();
			
			for (int i = 0; i < buttons.Length; i++)
			{
				if (buttons[i].name.Contains(ButtonName.Music.ToString()))
				{
					m_musicButton = buttons[i];

					buttons[i].onClick.AddListener(() => audioSettings.OnMusic(m_musicButton.transform));
				}
				else if (buttons[i].name.Contains(ButtonName.SoundEffect.ToString()))
				{
					m_soundEffectButton = buttons[i];

					buttons[i].onClick.AddListener(() => audioSettings.OnSoundEffects(m_soundEffectButton.transform));
				}
				else if (buttons[i].name.Contains(ButtonName.HowToPlay.ToString()))
				{
					m_howToPlayButton = buttons[i];

					buttons[i].onClick.AddListener(() => OpenPanel(m_howToPlayButton.transform, howToPlayPanelTransform));
				}
				else if (buttons[i].name.Contains(ButtonName.Credits.ToString()))
				{
					m_creditsButton = buttons[i];

					buttons[i].onClick.AddListener(() => OpenPanel(m_creditsButton.transform, creditsPanelTransform));
				}
				else if (buttons[i].name.Contains(ButtonName.Menu.ToString()))
				{
					m_backButton = buttons[i];

					buttons[i].onClick.AddListener(() => OnAction(ScreenName.Menu, m_backButton.transform));
				}
				else if (buttons[i].name.Contains(ButtonName.Close.ToString()))
				{
					if (buttons[i].transform.parent.name.Contains("HowToPlay"))
					{
						m_howToPlayCloseButton = buttons[i];

						buttons[i].onClick.AddListener(() => ClosePanel(m_howToPlayCloseButton.transform, howToPlayPanelTransform));
					}
					else if (buttons[i].transform.parent.name.Contains("Credits"))
					{
						m_creditsCloseButton = buttons[i];
						
						buttons[i].onClick.AddListener(() => ClosePanel(m_creditsCloseButton.transform, creditsPanelTransform));
					}
				}
				else if (buttons[i].name.Contains(ButtonName.Language.ToString()))
				{
					m_languageButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => 
					{
						if (m_enableLanguageButton)
						{
							m_enableLanguageButton = false;

							Translator.NextTranslation(m_languageButton.transform, SetFlag);
						}
					});
				}
			}
		}

		protected override void PrepareTexts ()
		{
			var settingsTranslation = Translator.CurrentTranslation.Settings;

			var dic = new Dictionary<string, string>()
			{
				{ "@SettingsTitle", settingsTranslation.Title },
				{ "@SettingsInstructions", settingsTranslation.Buttons.Instructions },
				{ "@SettingsCredits", settingsTranslation.Buttons.Credits },
				{ "@InstructionsTitle", settingsTranslation.Buttons.Instructions },
				{ "@InstructionsContent", string.Format(settingsTranslation.InstructionsPanel.Content, "<color=#EEBB12>", "</color>") },
				{ "@CreditsTitle", settingsTranslation.Buttons.Credits },
				{ "@CreditsContent", string.Format(settingsTranslation.CreditsPanel.Content, "<color=#EEBB12>", "</color>") }
			};
			
			Text[] texts = FindObjectsOfType<Text>();
			
			foreach (var item in texts) 
			{
				string message;
				if (dic.TryGetValue(item.name, out message))
				{
					item.text = message;
				}
			}
		}

		#endregion

		#region Methods (Private)

		void OpenPanel(Transform tr, Transform panel)
		{
			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayOpenPanel ();

			JKTween.AlphaColorImage (blurPainelImage, 0.5f, true, 1.25f);
			JKTween.ScaleTransform (panel, Vector3.zero, 1, 0.5f);
		}

		void ClosePanel(Transform tr, Transform panel)
		{
			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayClosePanel ();

			JKTween.AlphaColorImage (blurPainelImage, 0, false, 1.25f);
			JKTween.ScaleTransform (panel, Vector3.one, 0, 0.5f);
		}

		void SetFlag()
		{
			flagImage.sprite = flags [Translator.CurrentLanguage];

			PrepareTexts ();

			m_enableLanguageButton = true;
		}

		#endregion
	}
}