using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;
using JK.Integrations;

namespace Xenubi.Screens
{
	public class IntroScreen : GenericScreen 
	{
		[SerializeField] private Image[] logos;
		private int index = 0;

		protected override void Start ()
		{
			for (int i = 0; i < logos.Length; i++) 
			{
				logos[i].enabled = false;
				logos[i].color = new Color(1, 1, 1, 0);
			}
			
			Invoke ("ShowLogos", 2f);

			JKFacebookService.InitAPICall ();
		}


		void ShowLogos()
		{
			index = 0;

			ShowNextLogo ();
		}

		void ShowNextLogo()
		{
			JKTween.AlphaColorImage (logos [index], 1, true, 1f, () => 
			{
				index++;

				if (index < logos.Length)
				{
					ShowNextLogo();
				}
				else
				{
					OnAction(ScreenName.Menu, null);
				}
			});
		}

		#region implemented abstract members of GenericScreen

		protected override void BackButtonClick ()
		{
		}
		
		protected override void PrepareButtons ()
		{
		}
		
		protected override void PrepareTexts ()
		{
		}

		#endregion
	}
}