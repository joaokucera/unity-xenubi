using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;
using JK.Integrations;

namespace Xenubi.Screens
{
	public class LeaderboardScreen : GenericScreen 
	{
		private Button m_shareButton;
		private Button m_inviteButton;
		private Button m_backButton;

		#region Events

		protected override void Start()
		{
			base.Start ();

			if (FB.IsLoggedIn)
			{
				JKFacebookLeaderboard.SetLeaderboard (JKFacebookUserInfo.ScoresCollection);
			}
			else
			{
				OnAction(ScreenName.Menu);
			}
		}
		
		#endregion

		#region implemented abstract members of GenericScreen

		protected override void BackButtonClick ()
		{
			if (Input.GetKeyDown (KeyCode.Escape))
			{
				m_graphicRaycaster.enabled = false;

				OnAction(ScreenName.Menu, m_backButton.transform);
			}
		}

		protected override void PrepareButtons ()
		{
			Button[] buttons = GameObject.FindObjectsOfType<Button>();
			
			for (int i = 0; i < buttons.Length; i++)
			{
				if (buttons[i].name.Contains(ButtonName.Share.ToString()))
				{
					m_shareButton = buttons[i];

					buttons[i].onClick.AddListener(() => OnFacebookShare(m_shareButton.transform));
				}
				else if (buttons[i].name.Contains(ButtonName.Invite.ToString()))
				{
					m_inviteButton = buttons[i];

					buttons[i].onClick.AddListener(() => OnFacebookInvite(m_inviteButton.transform));
				}
				else if (buttons[i].name.Contains(ButtonName.Menu.ToString()))
				{
					m_backButton = buttons[i];

					buttons[i].onClick.AddListener(() => OnAction(ScreenName.Menu, m_backButton.transform));
				}
			}
		}

		protected override void PrepareTexts ()
		{
			var translation = Translator.CurrentTranslation;
			
			var dic = new Dictionary<string, string>()
			{
				{ "@LeaderboardTitle", translation.Leaderboard.Title },
				{ "@FacebookShare", translation.Facebook.Share },
				{ "@FacebookInvite", translation.Facebook.Invite }
			};
			
			Text[] texts = FindObjectsOfType<Text>();
			
			foreach (var item in texts) 
			{
				string message;
				if (dic.TryGetValue(item.name, out message))
				{
					item.text = message;
				}
			}
		}

		#endregion

		#region Methods

		void OnFacebookShare(Transform tr)
		{
			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayClickButton ();

			JKFacebookShare.OnShare ();
		}
		
		void OnFacebookInvite(Transform tr)
		{
			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayClickButton ();

			JKFacebookInvite.OnInvite ();
		}

		#endregion
	}
}