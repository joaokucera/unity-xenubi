using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;
using JK.Integrations;
using JK.Managment;

namespace Xenubi.Screens
{
	public class MenuScreen : GenericScreen 
	{
		#region Variables

		private PanelQuit m_quitPanel;
		private CanvasGroup m_canvasGroupFromLogo;

		private Button m_gameButton;
		private Button m_settingsButton;
		private Button m_leaderboardButton;
		private Button m_connectionButton;

		#endregion

		#region Events

		protected override void Awake ()
		{
			base.Awake ();

			m_canvasGroupFromLogo = GameObject.Find ("Canvas (logo only)").GetComponent<CanvasGroup> ();

			if (!JKManagment.IntroShowOff)
			{
				var logoTransform = GameObject.Find ("Logo Image (menu)").transform;

				JKTween.ScaleTransform (logoTransform, new Vector3 (2, 2, 2), 1, 2f);
				JKTween.RotateTransform (logoTransform, 2f, LogoHasArrived);
			}

			JKManagment.TutorialShowOff = false;
		}

		protected override void Start()
		{
			base.Start ();

			m_quitPanel = FindObjectOfType<PanelQuit> ();

			var facebookTranslation = Translator.CurrentTranslation.Facebook;
			JKFacebookConnection.SetWords (facebookTranslation.Connect, facebookTranslation.Disconnect);

			JKFacebookService.onMethodConnectHandler += JKFacebookConnection.OnConnect;
			JKFacebookService.onMethodDisconnectHandler += JKFacebookConnection.OnDisconnect;
			JKFacebookService.onMethodFlagHandler += LeaderboardButtonInteractableHandler;

			JKFacebookService.OnInitComplete();

			if (FB.IsLoggedIn)
			{
				JKFacebookProfile.SetProfileName ();
				JKFacebookProfile.SetProfilePicture ();
				
				JKFacebookConnection.ForceButtonConnected();
			}
		}

		#endregion

		#region implemented abstract members of GenericScreen

		protected override void BackButtonClick ()
		{
			if (JKManagment.IntroShowOff && !m_quitPanel.IsOpen && Input.GetKeyDown (KeyCode.Escape))
			{
				m_graphicRaycaster.enabled = false;
				
				m_quitPanel.OpenPanel(ScreenName.Quit, Translator.CurrentTranslation.QuitPanel.Content.GameMessage, null);
			}
		}

		protected override void PrepareMusic ()
		{
			if (JKManagment.IntroShowOff)
			{
				JKSoundManager.PlayMusic (musicClip);
			}
		}

		protected override void PrepareButtons ()
		{
			Button[] buttons = GameObject.FindObjectsOfType<Button>();

			for (int i = 0; i < buttons.Length; i++)
			{
				if (buttons[i].name.Contains(ButtonName.Game.ToString()))
				{
					m_gameButton = buttons[i];

					buttons[i].onClick.AddListener(() => 
					{
						OnAction(ScreenName.Game, m_gameButton.transform);

						JKTween.HideCanvasGroup (m_canvasGroupFromLogo, 2f);
					});
				}
				else if (buttons[i].name.Contains(ButtonName.Settings.ToString()))
				{
					m_settingsButton = buttons[i];

					buttons[i].onClick.AddListener(() => 
					{
						OnAction(ScreenName.Settings, m_settingsButton.transform);

						JKTween.HideCanvasGroup (m_canvasGroupFromLogo, 2f);
					});
				}
				else if (buttons[i].name.Contains(ButtonName.Leaderboard.ToString()))
				{
					m_leaderboardButton = buttons[i];

					buttons[i].onClick.AddListener(() => 
					{
						OnAction(ScreenName.Leaderboard, m_leaderboardButton.transform);

						JKTween.HideCanvasGroup (m_canvasGroupFromLogo, 2f);
					});
				}
				else if (buttons[i].name.Contains(ButtonName.Connection.ToString()))
				{
					m_connectionButton = buttons[i];

					buttons[i].onClick.AddListener(() => OnFacebookConnection(m_connectionButton.transform));
				}
			}
		}

		protected override void PrepareTexts ()
		{
			var translation = Translator.CurrentTranslation;

			var dic = new Dictionary<string, string>()
			{
				{ "@MenuPlay", translation.Menu.Buttons.Play },
				{ "@MenuSettings", translation.Menu.Buttons.Settings },
				{ "@MenuLeaderboard", translation.Menu.Buttons.Leaderboard },
				{ "@FacebookGuest", translation.Facebook.Guest },
				{ "@FacebookConnect", translation.Facebook.Connect },
				{ "@QuitPanelTitle", translation.QuitPanel.Title },
				{ "@QuitPanelNo", translation.QuitPanel.Buttons.No },
				{ "@QuitPanelYes", translation.QuitPanel.Buttons.Yes }
			};

			Text[] texts = FindObjectsOfType<Text>();

			foreach (var item in texts) 
			{
				string message;
				if (dic.TryGetValue(item.name, out message))
				{
					item.text = message;
				}
			}
		}

		#endregion

		#region Methods (Public)

		public void EnableGraphicRaycaster()
		{
			m_graphicRaycaster.enabled = true;
		}

		#endregion

		#region Methods (Private)

		void OnFacebookConnection(Transform tr)
		{
			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayClickButton ();

			JKFacebookService.ConnectionAPICall ();
		}

		void LeaderboardButtonInteractableHandler(bool isInteractable)
		{
			var go = GameObject.Find ("Leaderboard Button");

			if (go != null)
			{
			    var btn = go.GetComponent<Button>();

				if (btn != null)
				{
					btn.interactable = isInteractable;
				}
			}
		}
		
		void LogoHasArrived()
		{
			GameplayService.PlayImpact ();
			
			JKManagment.IntroShowOff = true;
			
			PrepareMusic ();
		}

		#endregion
	}
}