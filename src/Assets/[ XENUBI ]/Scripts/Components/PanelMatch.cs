using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;	
using JK.Effects;
using JK.Integrations;
using JK.Managment;
using Xenubi.Screens;
using System;

namespace Xenubi
{
	public class PanelMatch : MonoBehaviour 
	{
		private Button m_playButton;
		private PanelBlur m_blurPanel;
		private GameplayManager m_manager;
		private Toggle m_toggleHideWindow;

		[SerializeField] private Text textTitle;
		[SerializeField] private GameObject[] clues;
		[SerializeField] private Color victoryColor;
		[SerializeField] private Color defeatColor;

		public bool ShowWindow { get { return !m_toggleHideWindow.isOn; } }

		void Start()
		{
			Initialize ();
		}

		void Initialize ()
		{
			m_manager = FindObjectOfType<GameplayManager> ();
			m_blurPanel = FindObjectOfType<PanelBlur> ();

			transform.localScale = Vector3.zero;

			DesactiveClues ();
		}

		void DesactiveClues ()
		{
			for (int i = 0; i < clues.Length; i++)
			{
				clues [i].SetActive (false);
			}
		}

		public void Setup ()
		{
			if (m_manager == null || m_blurPanel == null)
			{
				Initialize();
			}

			PrepareButtons ();
		}

		void PrepareButtons ()
		{
			Button[] buttons = GetComponentsInChildren<Button>();
			
			for (int i = 0; i < buttons.Length; i++)
			{
				if (buttons[i].name.Contains(ButtonName.Play.ToString()))
				{
					m_playButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => ClosePanel(m_playButton.transform));
				}
			}

			m_toggleHideWindow = GetComponentInChildren<Toggle> ();
			m_toggleHideWindow.isOn = false;
			m_toggleHideWindow.onValueChanged.AddListener ((value) => ChangeToggleValue (value, m_toggleHideWindow.transform));
		}

		public void OpenPanel(bool isVictory, ElementType elementType, Action callback)
		{
			GameplayService.PlayOpenPanel ();
			clues[(int)elementType].SetActive(true);

			if (isVictory)
			{
				textTitle.text = Translator.CurrentTranslation.Game.MatchPanel.Victory;
				textTitle.color = victoryColor;
			}
			else
			{
				textTitle.text = Translator.CurrentTranslation.Game.MatchPanel.Defeat;
				textTitle.color = defeatColor;
			}

			m_blurPanel.OpenPanel ();
			JKTween.ScaleTransform (transform, Vector3.zero, 1, 0.5f, callback);
		}

		void ClosePanel(Transform tr)
		{
			m_manager.SetTableBackToContent ();

			DesactiveClues ();

			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayClosePanel ();
			
			m_blurPanel.ClosePanel ();
			JKTween.ScaleTransform (transform, Vector3.one, 0, 0.5f, () => 
			{
				m_manager.UpdateRound();
			});
		}
		
		bool ChangeToggleValue (bool value, Transform tr)
		{
			JKTween.ShakeScaleTransform (tr);

			return value;
		}
	}
}