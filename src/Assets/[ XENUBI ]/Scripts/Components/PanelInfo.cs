using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;	
using JK.Effects;
using JK.Integrations;
using JK.Managment;
using Xenubi.Screens;

namespace Xenubi
{
	public class PanelInfo : MonoBehaviour 
	{
		private Button m_playButton;

		private PanelBlur m_blurPanel;
		private GameplayManager m_manager;

		[SerializeField] private Text textMessage;

		void Start()
		{
			Initialize ();
		}

		void Initialize ()
		{
			m_manager = FindObjectOfType<GameplayManager> ();
			m_blurPanel = FindObjectOfType<PanelBlur> ();

			transform.localScale = Vector3.zero;
		}

		public void Setup ()
		{
			if (m_manager == null || m_blurPanel == null)
			{
				Initialize();
			}

			PrepareButtons ();
		}

		void PrepareButtons ()
		{
			Button[] buttons = GetComponentsInChildren<Button>();
			
			for (int i = 0; i < buttons.Length; i++)
			{
				if (buttons[i].name.Contains(ButtonName.Play.ToString()))
				{
					m_playButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => ClosePanel(m_playButton.transform));
				}
			}
		}

		public void OpenPanel(string message)
		{
			GameplayService.PlayOpenPanel ();

			textMessage.text = message;

			m_blurPanel.OpenPanel ();
			JKTween.ScaleTransform (transform, Vector3.zero, 1, 0.5f);
		}

		void ClosePanel(Transform tr)
		{
			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayClosePanel ();
			
			m_blurPanel.ClosePanel ();
			JKTween.ScaleTransform (transform, Vector3.one, 0, 0.5f, () => 
			{
				m_manager.ExecuteRound();
			});
		}
	}
}