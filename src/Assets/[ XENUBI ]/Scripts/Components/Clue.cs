﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;
using System;

namespace Xenubi
{
	public class Clue : MonoBehaviour 
	{
		private RectTransform m_rectTransform;

		private Text[] m_texts;
		private Image m_image;

		void Start()
		{
			Initialize ();
		}

		void Initialize()
		{
			m_rectTransform = transform as RectTransform;

			m_texts = GetComponentsInChildren<Text> ();
			m_image = GetComponentInChildren<Image> ();
		}

		public void Setup()
		{
			if (m_rectTransform == null || m_texts == null || m_image == null)
			{
				Initialize();
			}

			m_rectTransform.localPosition = new Vector3(-Screen.width * 2, 0, 0);

			gameObject.SetActive(false);
		}

		public void MoveRightOut()
		{
			JKTween.LocalMoveXTransform (m_rectTransform, Vector3.zero, Screen.width * 2, 1f);

			FadeOut ();
		}

		public void MoveRightIn(Action callback)
		{
			JKTween.LocalMoveXTransform (m_rectTransform, new Vector3(-Screen.width * 2, 0, 0), 0, 1f, callback);

			FadeIn ();
		}

		public void MoveLeftOut()
		{
			JKTween.LocalMoveXTransform (m_rectTransform, Vector3.zero, -Screen.width * 2, 1f);

			FadeOut ();
		}
		
		public void MoveLeftIn(Action callback)
		{
			JKTween.LocalMoveXTransform (m_rectTransform, new Vector3 (Screen.width * 2, 0, 0), 0, 1f, callback);

			FadeIn ();
		}

		void FadeIn()
		{
			JKTween.FadeImage(m_image, 0, 1, 1f);

			for (int i = 0; i < m_texts.Length; i++)
			{
				JKTween.FadeText(m_texts[i], 0, 1, 1f);
			}
		}

		void FadeOut()
		{
			JKTween.FadeImage (m_image, 1, 0, 0.25f);

			for (int i = 0; i < m_texts.Length; i++) 
			{
				JKTween.FadeText(m_texts[i], 1, 0, 0.25f);
			}
		}
	}
}