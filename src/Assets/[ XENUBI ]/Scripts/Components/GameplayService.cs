using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;
using JK.Effects;
using Random = UnityEngine.Random;

namespace Xenubi
{
	public class GameplayService : ScriptableObject 
	{
		#region Methods

//		public static List<Element> CreateElementsFromXml()
//		{
//			XDocument doc = XDocument.Parse (XML);
//
//			List<Element> list = doc.Descendants ("ChemistryElement").Select (c => new Element
//			{
//				Row = Convert.ToInt32(c.Element("row").Value),
//				Column = Convert.ToInt32(c.Element("column").Value),
//				Name = c.Element("name").Value,
//				Symbol = c.Element("symbol").Value,
//				AtomicNumber = Convert.ToInt32(c.Element("atomicNumber").Value),
//				AtomicMass = Convert.ToSingle(c.Element("atomicMass").Value),
//				AtomicRadius = Convert.ToSingle(c.Element("atomicRadius").Value),
//				ElectronNegativity = Convert.ToSingle(c.Element("eletroNegativity").Value),
//				IonizationEnergy = Convert.ToSingle(c.Element("ionizationEnergy").Value),
//				ElectronAffinity = Convert.ToSingle(c.Element("eletroAfinidade").Value),
//				Density = Convert.ToSingle(c.Element("density").Value),
//				PF = c.Element("PF").Value,
//				MeltingPoint = Convert.ToSingle(c.Element("fusionPoint").Value),
//				BoilingPoint = c.Element("boilingPoint").Value,
//				Abundance = c.Element("abundance").Value,
//			})
//			.OrderBy (c => c.Row)
//			.ToList<Element> ();
//			
//			return list;
//		}

		public static Element[] CreateListOfElements(string name, List<Element> elementsToUse, int quantity)
		{
			var elementsToCreate = new List<Element>();

			for (int i = 0; i < quantity; i++)
			{
				int index = Random.Range(0, elementsToUse.Count);
				
				elementsToCreate.Add(elementsToUse[index]);
				
				elementsToUse.RemoveAt(index);
			}

			//DebugListOfElements(name, elementsToCreate);

			return elementsToCreate.ToArray();
		}

		private static void DebugListOfElements(string name, List<Element> elements)
		{
			StringBuilder builder = new StringBuilder();

			builder.AppendLine(name);

			for (int i = 0; i < elements.Count; i++)
			{
				builder.AppendLine(elements[i].ToString());
			}

			builder.AppendLine ();

			Debug.Log(builder.ToString());
		}
		
		public static void PlayCardPlace ()
		{
			string[] cards = { "CardPlace1", "CardPlace1", "CardPlace1", "CardPlace1" };
			int index = Random.Range (0, cards.Length);
			
			JKSoundManager.PlaySoundEffect (cards[index]);
		}

		public static void PlayClickButton ()
		{
			string[] clicks = { "Click2", "Click3", "Click4", "Click5" };
			int index = Random.Range (0, clicks.Length);

			JKSoundManager.PlaySoundEffect (clicks[index]);
		}

		public static void PlayImpact ()
		{
			string[] impacts = { "Impact2", "Impact3", "Impact4", "Impact5", "Impact6" };
			int index = Random.Range (0, impacts.Length);

			JKSoundManager.PlaySoundEffect (impacts[index]);
		}

		public static void PlayPage ()
		{
			string[] pages = { "Page1", "Page2", "Page3" };
			int index = Random.Range (0, pages.Length);
			
			JKSoundManager.PlaySoundEffect (pages[index]);
		}
		
		public static void PlayClosePanel ()
		{
			JKSoundManager.PlaySoundEffect ("PopupClose");
		}

		public static void PlayOpenPanel ()
		{
			JKSoundManager.PlaySoundEffect ("PopupOpen");
		}

		// ROUND ???
		public static void PlayRoundResult (bool isVictory)
		{
			if (isVictory)
			{
				JKSoundManager.ActiveMusicFade (ExecuteVictoryRound, 5.3f);
			}
			else
			{
				JKSoundManager.ActiveMusicFade (ExecuteDefeatRound, 5.3f);
			}
		}
		// ROUND - Victory
		static void ExecuteVictoryRound ()
		{
			JKSoundManager.PlaySoundEffect ("VictoryRound");
		}
		// ROUND - Defeat
		static void ExecuteDefeatRound ()
		{
			JKSoundManager.PlaySoundEffect ("DefeatRound");
		}

		// MATCH ???
		public static void PlayMatchResult (bool isVictory)
		{
			if (isVictory)
			{
				JKSoundManager.ActiveMusicFade (ExecuteVictoryMatch, 2.3f);
			}
			else
			{
				JKSoundManager.ActiveMusicFade (ExecuteDefeatMatch, 2.3f);
			}
		}
		// MATCH - Defeat
		static void ExecuteVictoryMatch ()
		{
			JKSoundManager.PlaySoundEffect ("VictoryMatch");
		}
		// MATCH - Defeat
		static void ExecuteDefeatMatch ()
		{
			JKSoundManager.PlaySoundEffect ("DefeatMatch");
		}

		public static void PlayScore ()
		{
			JKSoundManager.PlaySoundEffect ("Score");
		}
		
		public static void PlayCustom (string clipName)
		{
			JKSoundManager.PlaySoundEffect (clipName);
		}

		#endregion
	}
}