using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;
using Xenubi;

[Serializable]
public class AudioSettings 
{
	[SerializeField] private Image musicIcon;
	[SerializeField] private Sprite musicSpriteEnabled;
	[SerializeField] private Sprite musicSpriteDisabled;
	
	[SerializeField] private Image soundEffectIcon;
	[SerializeField] private Sprite soundEffectSpriteEnabled;
	[SerializeField] private Sprite soundEffectSpriteDisabled;

	public void PrepareIcons ()
	{
		HandleMusicIcon ();

		HandleSoundEffectIcon ();
	}

	public void OnMusic(Transform tr)
	{
		JKTween.ShakeScaleTransform (tr);
		GameplayService.PlayClickButton ();
		
		JKSoundManager.ActiveMusic(HandleMusicIcon);
	}
	
	public void OnSoundEffects(Transform tr)
	{
		JKTween.ShakeScaleTransform (tr);
		GameplayService.PlayClickButton ();
		
		JKSoundManager.ActiveSoundEffects(HandleSoundEffectIcon);
	}

	void HandleMusicIcon()
	{
		musicIcon.sprite = JKSoundManager.MusicEnabled ? musicSpriteEnabled : musicSpriteDisabled;
	}

	void HandleSoundEffectIcon()
	{
		soundEffectIcon.sprite = JKSoundManager.SoundEffectsEnabled ? soundEffectSpriteEnabled : soundEffectSpriteDisabled;
	}
}