﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;

namespace Xenubi
{
	public class GameplayContent : MonoBehaviour 
	{
		public delegate void MethodRound(int victories, int defeats, bool addExtraScore);
		private MethodRound MethodRoundCallback;

		public delegate void MethodMatch(ElementType elementType, MatchResult matchResult);
		private MethodMatch MethodMatchCallback;

		private GameplayPeriodicTable m_table;
		private GameplayMatch[] m_matchs;
		private int m_currentMatch;
		private GameplayDeck m_playerDeck;
		private GameplayDeck m_enemyDeck;
		private Element[] m_currentPlayerElements = new Element[3];
		private Element[] m_currentEnemyElements = new Element[3];

		[SerializeField] private Sprite playerFeedbackSprite;
		[SerializeField] private Sprite enemyFeedbackSprite;
		
		public void SetTableParent (Transform parent) 
		{ 
			m_table.transform.SetParent (parent);
		}

		public void Setup (MethodRound roundCallback, MethodMatch matchCallback, List<Element> elementsToUse)
		{
			if (m_table == null || m_matchs == null || m_playerDeck == null || m_enemyDeck == null)
			{
				Initialize();
			}

			MethodRoundCallback = roundCallback;
			MethodMatchCallback = matchCallback;

			// MATCHS (animation)
			m_matchs [0].Show ();

			// Prepara a tabela e os elementos.
			m_table.Setup ();

			// Cria a lista do PLAYER.
			m_currentPlayerElements = GameplayService.CreateListOfElements("Player Elements", elementsToUse, 3);
			// Seta as cartas do PLAYER.
			m_playerDeck.Setup (m_currentPlayerElements, EvaluateClickedElementCallback);

			// Cria a lista do INIMIGO.
			m_currentEnemyElements = GameplayService.CreateListOfElements("Enemy Elements", elementsToUse, 3);
			// Seta as cartas do INIMIGO.
			m_enemyDeck.Setup (m_currentEnemyElements);
		}

		public void Play()
		{
			// Limpa a tabela.
			if (m_currentMatch > 0)
			{
				m_table.ClearData (m_currentPlayerElements [m_currentMatch - 1].Row, m_currentPlayerElements [m_currentMatch - 1].Column);
				m_table.ClearData (m_currentEnemyElements [m_currentMatch - 1].Row, m_currentEnemyElements [m_currentMatch - 1].Column);
			}

			// Dados do elemento atual do PLAYER.
			m_table.SetData (playerFeedbackSprite, m_currentPlayerElements [m_currentMatch].Row, m_currentPlayerElements [m_currentMatch].Column);
			// Dados do elemento atual do INIMIGO.
			m_table.SetData (enemyFeedbackSprite, m_currentEnemyElements [m_currentMatch].Row, m_currentEnemyElements [m_currentMatch].Column);

			// Mostra carta atual do PLAYER. 
			m_playerDeck.ShowCurrentCard (m_currentMatch);
			// Mostra carta atual do INIMIGO.
			m_enemyDeck.ShowCurrentCard (m_currentMatch);
		}

		void Hide(Action callback)
		{
			m_playerDeck.HideAllCards ();
			m_enemyDeck.HideAllCards ();

			m_matchs [0].Hide ();
			m_table.Hide(callback);
		}

		void Initialize()
		{
			m_playerDeck = transform.FindChild ("Deck Player").GetComponent<GameplayDeck> ();
			m_enemyDeck = transform.FindChild("Deck Enemy").GetComponent<GameplayDeck>();

			m_table = GetComponentInChildren<GameplayPeriodicTable> ();
			m_matchs = GetComponentsInChildren<GameplayMatch> ();
		}

		void EvaluateClickedElementCallback(ElementType elementType)
		{
			m_enemyDeck.MirrorDeck(m_currentMatch, (int)elementType, () =>
			{
				// Verifica se venceu.
				var matchResult = MatchElements (elementType);
				bool isVictory = matchResult != MatchResult.Defeat;

				GameplayService.PlayMatchResult (isVictory);

				// Define quem venceu a partida.
				Sprite other = isVictory ? playerFeedbackSprite : enemyFeedbackSprite;

				m_matchs[m_currentMatch].SetMatchResult(isVictory, elementType, other, () => 
				{
					MethodMatchCallback(elementType, matchResult);
				});
			});
		}

		public void UpdateMatch (Action callback)
		{
			// Esconde as cartas da partida.
			m_playerDeck.HideCurrentCard (m_currentMatch);
			m_enemyDeck.HideCurrentCard (m_currentMatch);

			// Pŕoxima partida.
			m_currentMatch++;
			bool lastMatch = m_currentMatch == m_matchs.Length;

			int victories = m_matchs.Count (m => m.IsPlayerVictory);
			bool earlyResult = m_currentMatch == 2 && victories % 2 == 0;

			if (lastMatch || earlyResult)
			{
				Hide (() =>  
				{
					// Final da rodada (verificaçao do placar).
					int defeats = m_currentMatch - victories;
					bool addExtraScore = defeats == 0;

					// Retorna os valores da rodada.
					MethodRoundCallback (victories, defeats, addExtraScore);
				});
			}
			else 
			{
				// Nova partida da mesma rodada.
				Play ();

				callback();
			}
		}

		MatchResult MatchElements(ElementType elementType)
		{
			MatchResult matchResult = MatchResult.Defeat;

			switch (elementType)
			{
				case ElementType.AtomicRadius:
				{
					if (m_currentPlayerElements[m_currentMatch].AtomicRadius > m_currentEnemyElements[m_currentMatch].AtomicRadius)
					{
						matchResult = MatchResult.Victory;
					}
					break;
				}
				case ElementType.IonizationEnergy:
				{
					if (m_currentPlayerElements[m_currentMatch].IonizationEnergy > m_currentEnemyElements[m_currentMatch].IonizationEnergy)
					{
						matchResult = MatchResult.Victory;
					}
					break;
				}
				case ElementType.ElectronAffinity:
				{
					if (m_currentPlayerElements[m_currentMatch].ElectronAffinity > m_currentEnemyElements[m_currentMatch].ElectronAffinity)
					{
						matchResult = MatchResult.VictoryCombo;
					}
					break;
				}
				case ElementType.ElectronNegativity:
				{
					if (m_currentPlayerElements[m_currentMatch].ElectronNegativity > m_currentEnemyElements[m_currentMatch].ElectronNegativity)
					{
						matchResult = MatchResult.VictoryCombo;
					}
					break;
				}
				case ElementType.MeltingPoint:
				{
					if (m_currentPlayerElements[m_currentMatch].MeltingPoint > m_currentEnemyElements[m_currentMatch].MeltingPoint)
					{
						matchResult = MatchResult.VictoryCombo;
					}
					break;
				}
				case ElementType.Density:
				{
					if (m_currentPlayerElements[m_currentMatch].Density > m_currentEnemyElements[m_currentMatch].Density)
					{
						matchResult = MatchResult.VictoryCombo;
					}
					break;
				}
			}

			return matchResult;
		}
	}
}