using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;
using JK.Patterns;

namespace Xenubi
{
	public class GameplayRound : MonoBehaviour
	{
		private const int WholeCardLevel = 1;
		private const int RandomPropertiesLevel = 2;
		private const int NoPropertiesLevel = 3;
		private const int OnlySymbolAndNameLevel = 5;
		private const int OnlySymbolLevel = 8;

		private Text m_roundText;
		private int m_currentRound;

		public int CurrentRound { get { return m_currentRound; } }

		public void Setup ()
		{
			m_roundText = GetComponentInChildren<Text> ();

			m_currentRound = 0;
		}

		public void OnStart(Action callback)
		{
			m_currentRound++;
			
			m_roundText.text = string.Format ("{0}ª {1}", m_currentRound, Translator.CurrentTranslation.Game.Header.Round);

			JKTween.PunchTransform (m_roundText.transform, Vector3.one, 1f, callback);
		}

		public RoundChallenge GetCurrentRoundChallenge()
		{
			if (m_currentRound >= OnlySymbolLevel)
			{
				return RoundChallenge.OnlySymbol;
			}
			else if (m_currentRound >= OnlySymbolAndNameLevel)
			{
				return RoundChallenge.OnlySymbolAndName;
			}
			else if (m_currentRound >= NoPropertiesLevel)
			{
				return RoundChallenge.NoProperties;
			}
			else if (m_currentRound >= RandomPropertiesLevel)
			{
				return RoundChallenge.RandomProperties;
			}
			else
			{
				return RoundChallenge.WholeCard;
			}
		}

		public bool GetCurrentRoundMessage(out string message)
		{
			message = string.Empty;

			if (m_currentRound == RandomPropertiesLevel)
			{
				message = string.Format(Translator.CurrentTranslation.Game.InfoPanel.Content.Round2, "<color=#EEBB12>", "</color>");

				return true;
			}
			else if (m_currentRound == NoPropertiesLevel)
			{
				message = string.Format(Translator.CurrentTranslation.Game.InfoPanel.Content.Round3, "<color=#EEBB12>", "</color>");

				return true;
			}
			else if (m_currentRound == OnlySymbolAndNameLevel)
			{
				message = string.Format(Translator.CurrentTranslation.Game.InfoPanel.Content.Round4, "<color=#EEBB12>", "</color>");

				return true;
			}
			else if (m_currentRound == OnlySymbolLevel)
			{
				message = string.Format(Translator.CurrentTranslation.Game.InfoPanel.Content.Round5, "<color=#EEBB12>", "</color>");

				return true;
			}

			return false;
		}
	}
}