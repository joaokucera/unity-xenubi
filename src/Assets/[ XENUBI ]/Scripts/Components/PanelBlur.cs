using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using JK.Effects;
using System;

namespace Xenubi
{
	public class PanelBlur : MonoBehaviour 
	{
		private Image m_blurImage;

		void Start()
		{
			m_blurImage = GetComponent<Image> ();

			m_blurImage.enabled = false;
			m_blurImage.color = new Color (1, 1, 1, 0);
		}

		public void OpenPanel(Action callback = null)
		{
			if (m_blurImage.color.a < 0.5f)
			{
				JKTween.AlphaColorImage (m_blurImage, 0.5f, true, 1.25f, callback);
			}
		}
		
		public void ClosePanel(Action callback = null)
		{
			if (m_blurImage.color.a > 0)
			{
				JKTween.AlphaColorImage (m_blurImage, 0, false, 1.25f, callback);
			}
		}
	}
}