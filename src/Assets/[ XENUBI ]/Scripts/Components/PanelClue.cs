using UnityEngine;
using System.Collections;
using JK.Effects;
using UnityEngine.UI;
using JK.Managment;
using JK.Integrations;

namespace Xenubi
{
	public class PanelClue : MonoBehaviour 
	{
		private Button m_closeButton;
		private Button m_previousButton;
		private Button m_nextButton;

		private bool m_canChangeClue = true;
		private int m_currentClue;
		[SerializeField] private Clue[] clues;

		void Start()
		{
			transform.localScale = Vector3.zero;

			for (int i = 1; i < clues.Length; i++) 
			{
				clues[i].Setup();
			}

			PrepareButtons ();
		}

		void PrepareButtons ()
		{
			Button[] buttons = GetComponentsInChildren<Button>();
			
			for (int i = 0; i < buttons.Length; i++)
			{
				if (buttons[i].name.Contains(ButtonName.Close.ToString()))
				{
					m_closeButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => ClosePanel(m_closeButton.transform));
				}
				else if (buttons[i].name.Contains(ButtonName.Previous.ToString()))
				{
					m_previousButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => 
					{
						if (m_canChangeClue)
						{
							PreviousClue(m_previousButton.transform);
						}
					});
				}
				else if (buttons[i].name.Contains(ButtonName.Next.ToString()))
				{
					m_nextButton = buttons[i];

					buttons[i].onClick.AddListener(() => 
					{
						if (m_canChangeClue)
						{
							NextClue(m_nextButton.transform);
						}
					});
				}
			}
		}

		void PreviousClue(Transform tr)
		{
			m_canChangeClue = false;
			JKTween.ShakeScaleTransform (tr);

			clues [m_currentClue].MoveLeftOut ();
			GameplayService.PlayPage();

			m_currentClue--;
			if (m_currentClue < 0)
			{
				m_currentClue = clues.Length - 1;
			}

			clues [m_currentClue].MoveLeftIn (() => m_canChangeClue = true);
		}

		void NextClue(Transform tr)
		{
			m_canChangeClue = false;
			JKTween.ShakeScaleTransform (tr);

			clues [m_currentClue].MoveRightOut ();
			GameplayService.PlayPage();

			m_currentClue++;
			if (m_currentClue >= clues.Length)
			{
				m_currentClue = 0;
			}

			clues [m_currentClue].MoveRightIn (() => m_canChangeClue = true);
		}

		public void OpenPanel(Transform tr)
		{
			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayOpenPanel ();

			JKTween.ScaleTransform (transform, Vector3.zero, 1, 0.5f, () =>
			{
				for (int i = 1; i < clues.Length; i++) 
				{
					clues[i].gameObject.SetActive(true);

					JKUnityAnalyticsService.SendClueOpen ();
				}
			});
		}
		
		void ClosePanel(Transform tr)
		{
			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayClosePanel ();
			
			JKTween.ScaleTransform (transform, Vector3.one, 0, 0.5f, () =>
			{
				for (int i = 1; i < clues.Length; i++) 
				{
					clues[i].gameObject.SetActive(false);
				}
			});
		}
	}
}