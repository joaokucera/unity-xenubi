using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;
using Xenubi.Screens;
using JK.Integrations;

namespace Xenubi
{
	public class GameplayTutorial : MonoBehaviour 
	{
		private GameScreen m_screen;
		private PanelBlur m_blurPanel;

		private bool m_canClick;

		private int m_currentStep;
		[SerializeField] private Step[] steps;

		[SerializeField] private TutorialButton m_previousTutorialButton;
		[SerializeField] private TutorialButton m_skipTutorialButton;
		[SerializeField] private TutorialButton m_nextTutorialButton;

		void Start()
		{
			m_screen = FindObjectOfType<GameScreen> ();
			m_blurPanel = FindObjectOfType<PanelBlur> ();

			var value = 4f;

			steps[0].Setup(TweenAxis.Y, new Vector3(steps[0].transform.localPosition.x, Screen.height * (value + 1f), steps[0].transform.localPosition.z));
			steps[1].Setup(TweenAxis.Y, new Vector3(steps[1].transform.localPosition.x, -Screen.height * value, steps[1].transform.localPosition.z));
			steps[2].Setup(TweenAxis.X, new Vector3(-Screen.width * value, steps[2].transform.localPosition.y, steps[2].transform.localPosition.z));
			steps[3].Setup(TweenAxis.X, new Vector3(Screen.width * value, steps[3].transform.localPosition.y, steps[3].transform.localPosition.z));

			PrepareButtons ();
		}

		void Update()
		{
			m_previousTutorialButton.InteractWith (m_currentStep > 0);
			m_nextTutorialButton.InteractWith (m_currentStep < steps.Length);
		}

		public void Show ()
		{
			m_skipTutorialButton.MoveIn (Screen.height * 2, TweenAxis.Y);

			m_previousTutorialButton.MoveIn (-Screen.width * 2, TweenAxis.X);

			m_nextTutorialButton.MoveIn (Screen.width * 2, TweenAxis.X, () => 
			{
				FirstStep();

				JKUnityAnalyticsService.SendTutorialOpen ();
			});
		}

		void PrepareButtons ()
		{
			m_skipTutorialButton.Setup(() => 
			{
				if (m_canClick) OnSkip(m_skipTutorialButton.transform);
			});

			m_previousTutorialButton.Setup(() => 
			{
				if (m_canClick && m_currentStep > 0) PreviousStep(m_previousTutorialButton.transform);
			});

			m_nextTutorialButton.Setup(() => 
			{
				if (m_canClick) NextStep(m_nextTutorialButton.transform);
			});
		}

		void FirstStep()
		{
			m_canClick = false;

			steps [m_currentStep].MoveIn (() => m_canClick = true);
		}

		void NextStep(Transform tr)
		{
			m_canClick = false;

			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayClickButton();

			steps[m_currentStep].ChangeParent(transform, true);

			m_currentStep++;
			if (m_currentStep >= steps.Length)
			{
				OnClose();
			}
			else
			{
				steps [m_currentStep].MoveIn (() => m_canClick = true);
			}
		}

		void PreviousStep (Transform tr)
		{
			m_canClick = false;
			
			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayClickButton();
			
			steps [m_currentStep].MoveOut ();

			m_currentStep--;
			if (m_currentStep >= 0)
			{
				steps [m_currentStep].MoveIn (() => m_canClick = true);
			}
		}

		void OnSkip(Transform tr)
		{
			m_canClick = false;

			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayClickButton();

			OnClose ();
		}

		void OnClose ()
		{
			for (int i = 0; i < steps.Length; i++) 
			{
				steps [i].MoveOut ();
			}

			m_skipTutorialButton.MoveOut (-Screen.height * 2, TweenAxis.Y);
			m_previousTutorialButton.MoveOut (-Screen.width * 2, TweenAxis.X);
			m_nextTutorialButton.MoveOut (Screen.width * 2, TweenAxis.X);

			m_blurPanel.ClosePanel (() => m_screen.StartGame());
		}
	}
}