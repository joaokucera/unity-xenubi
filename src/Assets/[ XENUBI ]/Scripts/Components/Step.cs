﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;

namespace Xenubi
{
	public class Step : MonoBehaviour 
	{
		private Transform m_originalParent;
		private RectTransform m_rectTransform;

		private Text[] m_texts;

		private TweenAxis m_axis;
		private Vector3 m_positionInside;
		private Vector3 m_positionOutside;

		[SerializeField] private RectTransform tutorialHelper;
		private Vector3 m_helperPositionInside;
		private Vector3 m_helperPositionOutside;

		private bool m_alreadyIn;

		void Start()
		{
			Initialize ();
		}
		
		void Initialize()
		{
			m_originalParent = transform.parent;
			m_rectTransform = transform as RectTransform;

			m_texts = GetComponentsInChildren<Text> ();

			FadeOut ();
		}
		
		public void Setup(TweenAxis axis, Vector3 outside)
		{
			if (m_rectTransform == null || m_texts == null)
			{
				Initialize();
			}

			m_axis = axis;

			m_positionInside = m_rectTransform.localPosition;
			m_rectTransform.localPosition = outside;
			m_positionOutside = m_rectTransform.localPosition;

			m_helperPositionInside = tutorialHelper.localPosition;
			tutorialHelper.localPosition = m_axis == TweenAxis.X 
				? new Vector3(-outside.x, tutorialHelper.localPosition.y, tutorialHelper.localPosition.z)
				: new Vector3(tutorialHelper.localPosition.x, -outside.y, tutorialHelper.localPosition.z);
			m_helperPositionOutside = tutorialHelper.localPosition;
		}

		public void MoveIn(Action callback)
		{
			m_alreadyIn = true;

			ChangeParent(m_originalParent, false);

			if (m_axis == TweenAxis.X)
			{
				JKTween.LocalMoveXTransform (m_rectTransform, m_rectTransform.localPosition.x, m_positionInside.x, 1f, callback);
				JKTween.LocalMoveXTransform (tutorialHelper, tutorialHelper.localPosition.x, m_helperPositionInside.x, 1f, () => tutorialHelper.SendMessage ("Show", callback));
			}
			else
			{
				JKTween.LocalMoveYTransform (m_rectTransform, m_rectTransform.localPosition.y, m_positionInside.y, 1f, callback);
				JKTween.LocalMoveYTransform (tutorialHelper, tutorialHelper.localPosition.y, m_helperPositionInside.y, 1f, () => tutorialHelper.SendMessage ("Show", callback));
			}

			FadeIn ();
		}

		public void MoveOut(Action callback = null)
		{
			if (m_alreadyIn)
			{
				if (m_axis == TweenAxis.X)
				{
					JKTween.LocalMoveXTransform (m_rectTransform, m_rectTransform.localPosition.x, m_positionOutside.x, 1f, callback);
					JKTween.LocalMoveXTransform (tutorialHelper, tutorialHelper.localPosition.x, m_helperPositionOutside.x, 1f, callback);
				}
				else
				{
					JKTween.LocalMoveYTransform (m_rectTransform, m_rectTransform.localPosition.y, m_positionOutside.y, 1f, callback);
					JKTween.LocalMoveYTransform (tutorialHelper, tutorialHelper.localPosition.y, m_helperPositionOutside.y, 1f, callback);
				}
			}
			else
			{
				if (callback != null) callback();
			}
		}

		void FadeIn()
		{
			for (int i = 0; i < m_texts.Length; i++)
			{
				JKTween.FadeText(m_texts[i], 0, 1, 0.5f);
			}
		}

		void FadeOut()
		{
			for (int i = 0; i < m_texts.Length; i++) 
			{
				JKTween.FadeText(m_texts[i], 1, 0, 0.25f);
			}
		}

		public void ChangeParent (Transform newParent, bool moveHelper)
		{
			transform.SetParent (newParent);

			tutorialHelper.SetParent (newParent);

			if (moveHelper)
			{
				if (m_axis == TweenAxis.X)
				{
					JKTween.LocalMoveXTransform (tutorialHelper, tutorialHelper.localPosition.x, m_helperPositionOutside.x, 1f);
				}
				else
				{
					JKTween.LocalMoveYTransform (tutorialHelper, tutorialHelper.localPosition.y, m_helperPositionOutside.y, 1f);
				}

				tutorialHelper.SendMessage ("Finish");
			}
		}
	}
}
