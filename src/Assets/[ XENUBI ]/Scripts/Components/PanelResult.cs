using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;	
using JK.Effects;
using JK.Integrations;
using JK.Managment;
using Xenubi.Screens;

namespace Xenubi
{
	public class PanelResult : MonoBehaviour 
	{
		private Button m_menuButton;
		private Button m_reloadButton;
		private Button m_shareButton;
		private Button m_connectionButton;

		private PanelBlur m_blurPanel;
		private GameScreen m_screen;
		private int m_currentScore;

		private Text m_textBestScore;
		public Text TextBestScore 
		{ 
			get 
			{ 
				if (m_textBestScore == null)
				{
					m_textBestScore = transform.FindChild("Content/Best Score Text").GetComponent<Text>();
				}

				return m_textBestScore; 
			} 
		}

		private Text m_textFinalScore;
		public Text TextFinalScore 
		{ 
			get 
			{ 
				if (m_textFinalScore == null)
				{
					m_textFinalScore = transform.FindChild("Content/Final Score Text").GetComponent<Text>();
				}
				
				return m_textFinalScore; 
			} 
		}

		private GameObject m_connectionButtonObject;
		public GameObject ConnectionButtonObject 
		{ 
			get 
			{ 
				if (m_connectionButtonObject == null)
				{
					m_connectionButtonObject = transform.FindChild("Connection Button").gameObject;
				}
				
				return m_connectionButtonObject; 
			} 
		}

		private GameObject m_shareButtonObject;
		public GameObject ShareButtonObject 
		{ 
			get 
			{ 
				if (m_shareButtonObject == null)
				{
					m_shareButtonObject = transform.FindChild("Share Button").gameObject;
				}
				
				return m_shareButtonObject; 
			} 
		}

		void Start()
		{
			Initialize ();
		}

		void Initialize ()
		{
			m_screen = FindObjectOfType<GameScreen> ();
			m_blurPanel = FindObjectOfType<PanelBlur> ();

			transform.localScale = Vector3.zero;

			TextBestScore.text = string.Empty;
			TextFinalScore.text = string.Empty;
		}

		public void Setup ()
		{
			if (m_screen == null || m_blurPanel == null)
			{
				Initialize();
			}

			PrepareButtons ();

			FacebookButtonsHandler(FB.IsLoggedIn);
		}

		void PrepareButtons ()
		{
			Button[] buttons = GetComponentsInChildren<Button>();
			
			for (int i = 0; i < buttons.Length; i++)
			{
				if (buttons[i].name.Contains(ButtonName.Menu.ToString()))
				{
					StopAllCoroutines();

					m_menuButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => m_screen.OnAction(ScreenName.Menu, m_menuButton.transform));
				}
				else if (buttons[i].name.Contains(ButtonName.Reload.ToString()))
				{
					StopAllCoroutines();

					m_reloadButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => m_screen.OnAction(ScreenName.Game, m_reloadButton.transform));
				}
				else if (buttons[i].name.Contains(ButtonName.Share.ToString()))
				{
					StopAllCoroutines();

					m_shareButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => OnFacebookShare(m_shareButton.transform));
				}
				else if (buttons[i].name.Contains(ButtonName.Connection.ToString()))
				{
					StopAllCoroutines();

					m_connectionButton = buttons[i];
					
					buttons[i].onClick.AddListener(() => OnFacebookConnection(m_connectionButton.transform));
				}
			}
		}

		void OnFacebookShare(Transform tr)
		{
			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayClickButton ();
			
			JKFacebookShare.OnShareResult (m_currentScore);
		}

		void OnFacebookConnection(Transform tr)
		{
			JKTween.ShakeScaleTransform (tr);
			GameplayService.PlayClickButton ();

			JKFacebookService.onMethodFlagHandler += FacebookButtonsHandler;
			JKFacebookService.ConnectionAPICall ();
		}

		void FacebookButtonsHandler(bool isLogged)
		{
			m_screen.SetupFacebookProfile (isLogged);

			if (isLogged)
			{
				ShareButtonObject.SetActive(true);
				ConnectionButtonObject.SetActive (false);
			}
			else
			{
				ShareButtonObject.SetActive(false);
				ConnectionButtonObject.SetActive (true);
			}
		}

		public void OpenPanel(int scoreResult)
		{
			GameplayService.PlayOpenPanel ();

			m_blurPanel.OpenPanel ();
			JKTween.ScaleTransform (transform, Vector3.zero, 1, 0.5f, () => ShowScoreResult(scoreResult));
		}

		void ShowScoreResult(int scoreResult)
		{
			m_currentScore = scoreResult;

			StartCoroutine (UpdateScoreResult ());
		}

		IEnumerator UpdateScoreResult()
		{
			string scoreText = Translator.CurrentTranslation.Game.ResultPanel.Score;
			if (m_currentScore > 1) scoreText += "s";

			var partialScore = 0;
			var sum = m_currentScore / GameplayManager.VictoryPoints;

			while (partialScore < m_currentScore)
			{
				partialScore += sum;
				TextFinalScore.text = string.Format("<color=#EEBB12>{0} {1}</color>", partialScore, scoreText);

				Tween tween = JKTween.PunchTransform (TextFinalScore.transform, Vector2.one / 4, 0.25f, () =>
				{
					GameplayService.PlayScore();
				});

				yield return tween.WaitForCompletion();
			}

			TextFinalScore.text = string.Format("{0}: <color=#EEBB12>{1} {2}</color>", Translator.CurrentTranslation.Game.ResultPanel.FinalScore, m_currentScore, scoreText);

			JKTween.PunchTransform(TextFinalScore.transform, Vector2.one / 2, 0.5f, () => 
			{
				int previous = JKFacebookUserInfo.PreviousScore;
				int best = m_currentScore > previous ? m_currentScore : previous;

				string bestText = Translator.CurrentTranslation.Game.ResultPanel.Score;
				if (best > 1) bestText += "s";

				TextBestScore.text = string.Format("{0}: <color=#EEBB12>{1} {2}</color>", Translator.CurrentTranslation.Game.ResultPanel.BestScore, best, bestText);

				JKTween.PunchTransform (TextBestScore.transform, Vector2.one / 4, 0.5f);
			});
		}
	}
}