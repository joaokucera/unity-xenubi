﻿
/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class Translation
{

    private TranslationFacebook facebookField;

    private TranslationMenu menuField;

    private TranslationSettings settingsField;

    private TranslationLeaderboard leaderboardField;

    private TranslationGame gameField;

    private TranslationQuitPanel quitPanelField;

    /// <remarks/>
    public TranslationFacebook Facebook
    {
        get
        {
            return this.facebookField;
        }
        set
        {
            this.facebookField = value;
        }
    }

    /// <remarks/>
    public TranslationMenu Menu
    {
        get
        {
            return this.menuField;
        }
        set
        {
            this.menuField = value;
        }
    }

    /// <remarks/>
    public TranslationSettings Settings
    {
        get
        {
            return this.settingsField;
        }
        set
        {
            this.settingsField = value;
        }
    }

    /// <remarks/>
    public TranslationLeaderboard Leaderboard
    {
        get
        {
            return this.leaderboardField;
        }
        set
        {
            this.leaderboardField = value;
        }
    }

    /// <remarks/>
    public TranslationGame Game
    {
        get
        {
            return this.gameField;
        }
        set
        {
            this.gameField = value;
        }
    }

    /// <remarks/>
    public TranslationQuitPanel QuitPanel
    {
        get
        {
            return this.quitPanelField;
        }
        set
        {
            this.quitPanelField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationFacebook
{

    private string guestField;

    private string connectField;

    private string disconnectField;

    private string shareField;

    private string shareTitleField;

    private string shareResultField;

    private string shareImageUrlField;

    private string shareCaptionField;

    private string inviteField;

    private string inviteTitleField;

    private string inviteTextField;

    private string inviteResultField;

    private string inviteChallengeField;

    private string invitePointsField;

    private string inviteMessageField;

    /// <remarks/>
    public string Guest
    {
        get
        {
            return this.guestField;
        }
        set
        {
            this.guestField = value;
        }
    }

    /// <remarks/>
    public string Connect
    {
        get
        {
            return this.connectField;
        }
        set
        {
            this.connectField = value;
        }
    }

    /// <remarks/>
    public string Disconnect
    {
        get
        {
            return this.disconnectField;
        }
        set
        {
            this.disconnectField = value;
        }
    }

    /// <remarks/>
    public string Share
    {
        get
        {
            return this.shareField;
        }
        set
        {
            this.shareField = value;
        }
    }

    /// <remarks/>
    public string ShareTitle
    {
        get
        {
            return this.shareTitleField;
        }
        set
        {
            this.shareTitleField = value;
        }
    }

    /// <remarks/>
    public string ShareResult
    {
        get
        {
            return this.shareResultField;
        }
        set
        {
            this.shareResultField = value;
        }
    }

    /// <remarks/>
    public string ShareImageUrl
    {
        get
        {
            return this.shareImageUrlField;
        }
        set
        {
            this.shareImageUrlField = value;
        }
    }

    /// <remarks/>
    public string ShareCaption
    {
        get
        {
            return this.shareCaptionField;
        }
        set
        {
            this.shareCaptionField = value;
        }
    }

    /// <remarks/>
    public string Invite
    {
        get
        {
            return this.inviteField;
        }
        set
        {
            this.inviteField = value;
        }
    }

    /// <remarks/>
    public string InviteTitle
    {
        get
        {
            return this.inviteTitleField;
        }
        set
        {
            this.inviteTitleField = value;
        }
    }

    /// <remarks/>
    public string InviteText
    {
        get
        {
            return this.inviteTextField;
        }
        set
        {
            this.inviteTextField = value;
        }
    }

    /// <remarks/>
    public string InviteResult
    {
        get
        {
            return this.inviteResultField;
        }
        set
        {
            this.inviteResultField = value;
        }
    }

    /// <remarks/>
    public string InviteChallenge
    {
        get
        {
            return this.inviteChallengeField;
        }
        set
        {
            this.inviteChallengeField = value;
        }
    }

    /// <remarks/>
    public string InvitePoints
    {
        get
        {
            return this.invitePointsField;
        }
        set
        {
            this.invitePointsField = value;
        }
    }

    /// <remarks/>
    public string InviteMessage
    {
        get
        {
            return this.inviteMessageField;
        }
        set
        {
            this.inviteMessageField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationMenu
{

    private TranslationMenuButtons buttonsField;

    /// <remarks/>
    public TranslationMenuButtons Buttons
    {
        get
        {
            return this.buttonsField;
        }
        set
        {
            this.buttonsField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationMenuButtons
{

    private string playField;

    private string settingsField;

    private string leaderboardField;

    /// <remarks/>
    public string Play
    {
        get
        {
            return this.playField;
        }
        set
        {
            this.playField = value;
        }
    }

    /// <remarks/>
    public string Settings
    {
        get
        {
            return this.settingsField;
        }
        set
        {
            this.settingsField = value;
        }
    }

    /// <remarks/>
    public string Leaderboard
    {
        get
        {
            return this.leaderboardField;
        }
        set
        {
            this.leaderboardField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationSettings
{

    private string titleField;

    private TranslationSettingsButtons buttonsField;

    private TranslationSettingsInstructionsPanel instructionsPanelField;

    private TranslationSettingsCreditsPanel creditsPanelField;

    /// <remarks/>
    public string Title
    {
        get
        {
            return this.titleField;
        }
        set
        {
            this.titleField = value;
        }
    }

    /// <remarks/>
    public TranslationSettingsButtons Buttons
    {
        get
        {
            return this.buttonsField;
        }
        set
        {
            this.buttonsField = value;
        }
    }

    /// <remarks/>
    public TranslationSettingsInstructionsPanel InstructionsPanel
    {
        get
        {
            return this.instructionsPanelField;
        }
        set
        {
            this.instructionsPanelField = value;
        }
    }

    /// <remarks/>
    public TranslationSettingsCreditsPanel CreditsPanel
    {
        get
        {
            return this.creditsPanelField;
        }
        set
        {
            this.creditsPanelField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationSettingsButtons
{

    private string instructionsField;

    private string creditsField;

    /// <remarks/>
    public string Instructions
    {
        get
        {
            return this.instructionsField;
        }
        set
        {
            this.instructionsField = value;
        }
    }

    /// <remarks/>
    public string Credits
    {
        get
        {
            return this.creditsField;
        }
        set
        {
            this.creditsField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationSettingsInstructionsPanel
{

    private string contentField;

    /// <remarks/>
    public string Content
    {
        get
        {
            return this.contentField;
        }
        set
        {
            this.contentField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationSettingsCreditsPanel
{

    private string contentField;

    /// <remarks/>
    public string Content
    {
        get
        {
            return this.contentField;
        }
        set
        {
            this.contentField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationLeaderboard
{

    private string titleField;

    private string scoreField;

    /// <remarks/>
    public string Title
    {
        get
        {
            return this.titleField;
        }
        set
        {
            this.titleField = value;
        }
    }

    /// <remarks/>
    public string Score
    {
        get
        {
            return this.scoreField;
        }
        set
        {
            this.scoreField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGame
{

    private string readyMessageField;

    private TranslationGameHeader headerField;

    private TranslationGamePeriodicTable periodicTableField;

    private TranslationGamePausePanel pausePanelField;

    private TranslationGameTipPanel tipPanelField;

    private TranslationGameTutorialPanel tutorialPanelField;

    private TranslationGameMatchPanel matchPanelField;

    private TranslationGameInfoPanel infoPanelField;

    private TranslationGameResultPanel resultPanelField;

    /// <remarks/>
    public string ReadyMessage
    {
        get
        {
            return this.readyMessageField;
        }
        set
        {
            this.readyMessageField = value;
        }
    }

    /// <remarks/>
    public TranslationGameHeader Header
    {
        get
        {
            return this.headerField;
        }
        set
        {
            this.headerField = value;
        }
    }

    /// <remarks/>
    public TranslationGamePeriodicTable PeriodicTable
    {
        get
        {
            return this.periodicTableField;
        }
        set
        {
            this.periodicTableField = value;
        }
    }

    /// <remarks/>
    public TranslationGamePausePanel PausePanel
    {
        get
        {
            return this.pausePanelField;
        }
        set
        {
            this.pausePanelField = value;
        }
    }

    /// <remarks/>
    public TranslationGameTipPanel TipPanel
    {
        get
        {
            return this.tipPanelField;
        }
        set
        {
            this.tipPanelField = value;
        }
    }

    /// <remarks/>
    public TranslationGameTutorialPanel TutorialPanel
    {
        get
        {
            return this.tutorialPanelField;
        }
        set
        {
            this.tutorialPanelField = value;
        }
    }

    /// <remarks/>
    public TranslationGameMatchPanel MatchPanel
    {
        get
        {
            return this.matchPanelField;
        }
        set
        {
            this.matchPanelField = value;
        }
    }

    /// <remarks/>
    public TranslationGameInfoPanel InfoPanel
    {
        get
        {
            return this.infoPanelField;
        }
        set
        {
            this.infoPanelField = value;
        }
    }

    /// <remarks/>
    public TranslationGameResultPanel ResultPanel
    {
        get
        {
            return this.resultPanelField;
        }
        set
        {
            this.resultPanelField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGameHeader
{

    private string roundField;

    private string timerField;

    /// <remarks/>
    public string Round
    {
        get
        {
            return this.roundField;
        }
        set
        {
            this.roundField = value;
        }
    }

    /// <remarks/>
    public string Timer
    {
        get
        {
            return this.timerField;
        }
        set
        {
            this.timerField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGamePeriodicTable
{

    private string atomicRadiusField;

    private string ionizationEnergyField;

    private string electronAffinityField;

    private string electronNegativityField;

    private string meltingPointField;

    private string densityField;

    /// <remarks/>
    public string AtomicRadius
    {
        get
        {
            return this.atomicRadiusField;
        }
        set
        {
            this.atomicRadiusField = value;
        }
    }

    /// <remarks/>
    public string IonizationEnergy
    {
        get
        {
            return this.ionizationEnergyField;
        }
        set
        {
            this.ionizationEnergyField = value;
        }
    }

    /// <remarks/>
    public string ElectronAffinity
    {
        get
        {
            return this.electronAffinityField;
        }
        set
        {
            this.electronAffinityField = value;
        }
    }

    /// <remarks/>
    public string ElectronNegativity
    {
        get
        {
            return this.electronNegativityField;
        }
        set
        {
            this.electronNegativityField = value;
        }
    }

    /// <remarks/>
    public string MeltingPoint
    {
        get
        {
            return this.meltingPointField;
        }
        set
        {
            this.meltingPointField = value;
        }
    }

    /// <remarks/>
    public string Density
    {
        get
        {
            return this.densityField;
        }
        set
        {
            this.densityField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGamePausePanel
{

    private string titleField;

    private string tipField;

    /// <remarks/>
    public string Title
    {
        get
        {
            return this.titleField;
        }
        set
        {
            this.titleField = value;
        }
    }

    /// <remarks/>
    public string Tip
    {
        get
        {
            return this.tipField;
        }
        set
        {
            this.tipField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGameTipPanel
{

    private string titleField;

    private TranslationGameTipPanelContent contentField;

    /// <remarks/>
    public string Title
    {
        get
        {
            return this.titleField;
        }
        set
        {
            this.titleField = value;
        }
    }

    /// <remarks/>
    public TranslationGameTipPanelContent Content
    {
        get
        {
            return this.contentField;
        }
        set
        {
            this.contentField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGameTipPanelContent
{

    private TranslationGameTipPanelContentAtomicRadius atomicRadiusField;

    private TranslationGameTipPanelContentIonizationEnergy ionizationEnergyField;

    private TranslationGameTipPanelContentElectronAffinity electronAffinityField;

    private TranslationGameTipPanelContentElectronNegativity electronNegativityField;

    private TranslationGameTipPanelContentMeltingPoint meltingPointField;

    private TranslationGameTipPanelContentDensity densityField;

    /// <remarks/>
    public TranslationGameTipPanelContentAtomicRadius AtomicRadius
    {
        get
        {
            return this.atomicRadiusField;
        }
        set
        {
            this.atomicRadiusField = value;
        }
    }

    /// <remarks/>
    public TranslationGameTipPanelContentIonizationEnergy IonizationEnergy
    {
        get
        {
            return this.ionizationEnergyField;
        }
        set
        {
            this.ionizationEnergyField = value;
        }
    }

    /// <remarks/>
    public TranslationGameTipPanelContentElectronAffinity ElectronAffinity
    {
        get
        {
            return this.electronAffinityField;
        }
        set
        {
            this.electronAffinityField = value;
        }
    }

    /// <remarks/>
    public TranslationGameTipPanelContentElectronNegativity ElectronNegativity
    {
        get
        {
            return this.electronNegativityField;
        }
        set
        {
            this.electronNegativityField = value;
        }
    }

    /// <remarks/>
    public TranslationGameTipPanelContentMeltingPoint MeltingPoint
    {
        get
        {
            return this.meltingPointField;
        }
        set
        {
            this.meltingPointField = value;
        }
    }

    /// <remarks/>
    public TranslationGameTipPanelContentDensity Density
    {
        get
        {
            return this.densityField;
        }
        set
        {
            this.densityField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGameTipPanelContentAtomicRadius
{

    private string sideMessageField;

    private string bottomMessageField;

    /// <remarks/>
    public string SideMessage
    {
        get
        {
            return this.sideMessageField;
        }
        set
        {
            this.sideMessageField = value;
        }
    }

    /// <remarks/>
    public string BottomMessage
    {
        get
        {
            return this.bottomMessageField;
        }
        set
        {
            this.bottomMessageField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGameTipPanelContentIonizationEnergy
{

    private string sideMessageField;

    private string bottomMessageField;

    /// <remarks/>
    public string SideMessage
    {
        get
        {
            return this.sideMessageField;
        }
        set
        {
            this.sideMessageField = value;
        }
    }

    /// <remarks/>
    public string BottomMessage
    {
        get
        {
            return this.bottomMessageField;
        }
        set
        {
            this.bottomMessageField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGameTipPanelContentElectronAffinity
{

    private string sideMessageField;

    private string bottomMessageField;

    /// <remarks/>
    public string SideMessage
    {
        get
        {
            return this.sideMessageField;
        }
        set
        {
            this.sideMessageField = value;
        }
    }

    /// <remarks/>
    public string BottomMessage
    {
        get
        {
            return this.bottomMessageField;
        }
        set
        {
            this.bottomMessageField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGameTipPanelContentElectronNegativity
{

    private string sideMessageField;

    private string bottomMessageField;

    /// <remarks/>
    public string SideMessage
    {
        get
        {
            return this.sideMessageField;
        }
        set
        {
            this.sideMessageField = value;
        }
    }

    /// <remarks/>
    public string BottomMessage
    {
        get
        {
            return this.bottomMessageField;
        }
        set
        {
            this.bottomMessageField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGameTipPanelContentMeltingPoint
{

    private string sideMessageField;

    private string bottomMessageField;

    /// <remarks/>
    public string SideMessage
    {
        get
        {
            return this.sideMessageField;
        }
        set
        {
            this.sideMessageField = value;
        }
    }

    /// <remarks/>
    public string BottomMessage
    {
        get
        {
            return this.bottomMessageField;
        }
        set
        {
            this.bottomMessageField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGameTipPanelContentDensity
{

    private string sideMessageField;

    private string bottomMessageField;

    /// <remarks/>
    public string SideMessage
    {
        get
        {
            return this.sideMessageField;
        }
        set
        {
            this.sideMessageField = value;
        }
    }

    /// <remarks/>
    public string BottomMessage
    {
        get
        {
            return this.bottomMessageField;
        }
        set
        {
            this.bottomMessageField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGameTutorialPanel
{

    private string titleField;

    private string questionField;

    private TranslationGameTutorialPanelButtons buttonsField;

    private TranslationGameTutorialPanelContent contentField;

    /// <remarks/>
    public string Title
    {
        get
        {
            return this.titleField;
        }
        set
        {
            this.titleField = value;
        }
    }

    /// <remarks/>
    public string Question
    {
        get
        {
            return this.questionField;
        }
        set
        {
            this.questionField = value;
        }
    }

    /// <remarks/>
    public TranslationGameTutorialPanelButtons Buttons
    {
        get
        {
            return this.buttonsField;
        }
        set
        {
            this.buttonsField = value;
        }
    }

    /// <remarks/>
    public TranslationGameTutorialPanelContent Content
    {
        get
        {
            return this.contentField;
        }
        set
        {
            this.contentField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGameTutorialPanelButtons
{

    private string noField;

    private string yesField;

    private string skipField;

    /// <remarks/>
    public string No
    {
        get
        {
            return this.noField;
        }
        set
        {
            this.noField = value;
        }
    }

    /// <remarks/>
    public string Yes
    {
        get
        {
            return this.yesField;
        }
        set
        {
            this.yesField = value;
        }
    }

    /// <remarks/>
    public string Skip
    {
        get
        {
            return this.skipField;
        }
        set
        {
            this.skipField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGameTutorialPanelContent
{

    private string roundMatchField;

    private string periodicTableField;

    private string deckPlayerField;

    private string deckEnemyField;

    /// <remarks/>
    public string RoundMatch
    {
        get
        {
            return this.roundMatchField;
        }
        set
        {
            this.roundMatchField = value;
        }
    }

    /// <remarks/>
    public string PeriodicTable
    {
        get
        {
            return this.periodicTableField;
        }
        set
        {
            this.periodicTableField = value;
        }
    }

    /// <remarks/>
    public string DeckPlayer
    {
        get
        {
            return this.deckPlayerField;
        }
        set
        {
            this.deckPlayerField = value;
        }
    }

    /// <remarks/>
    public string DeckEnemy
    {
        get
        {
            return this.deckEnemyField;
        }
        set
        {
            this.deckEnemyField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGameMatchPanel
{

    private string victoryField;

    private string defeatField;

    private string choiceField;

    private string toggleField;

    /// <remarks/>
    public string Victory
    {
        get
        {
            return this.victoryField;
        }
        set
        {
            this.victoryField = value;
        }
    }

    /// <remarks/>
    public string Defeat
    {
        get
        {
            return this.defeatField;
        }
        set
        {
            this.defeatField = value;
        }
    }

    /// <remarks/>
    public string Choice
    {
        get
        {
            return this.choiceField;
        }
        set
        {
            this.choiceField = value;
        }
    }

    /// <remarks/>
    public string Toggle
    {
        get
        {
            return this.toggleField;
        }
        set
        {
            this.toggleField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGameInfoPanel
{

    private string titleField;

    private TranslationGameInfoPanelContent contentField;

    /// <remarks/>
    public string Title
    {
        get
        {
            return this.titleField;
        }
        set
        {
            this.titleField = value;
        }
    }

    /// <remarks/>
    public TranslationGameInfoPanelContent Content
    {
        get
        {
            return this.contentField;
        }
        set
        {
            this.contentField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGameInfoPanelContent
{

    private object round1Field;

    private string round2Field;

    private string round3Field;

    private string round4Field;

    private string round5Field;

    /// <remarks/>
    public object Round1
    {
        get
        {
            return this.round1Field;
        }
        set
        {
            this.round1Field = value;
        }
    }

    /// <remarks/>
    public string Round2
    {
        get
        {
            return this.round2Field;
        }
        set
        {
            this.round2Field = value;
        }
    }

    /// <remarks/>
    public string Round3
    {
        get
        {
            return this.round3Field;
        }
        set
        {
            this.round3Field = value;
        }
    }

    /// <remarks/>
    public string Round4
    {
        get
        {
            return this.round4Field;
        }
        set
        {
            this.round4Field = value;
        }
    }

    /// <remarks/>
    public string Round5
    {
        get
        {
            return this.round5Field;
        }
        set
        {
            this.round5Field = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationGameResultPanel
{

    private string titleField;

    private string bestScoreField;

    private string finalScoreField;

    private string scoreField;

    /// <remarks/>
    public string Title
    {
        get
        {
            return this.titleField;
        }
        set
        {
            this.titleField = value;
        }
    }

    /// <remarks/>
    public string BestScore
    {
        get
        {
            return this.bestScoreField;
        }
        set
        {
            this.bestScoreField = value;
        }
    }

    /// <remarks/>
    public string FinalScore
    {
        get
        {
            return this.finalScoreField;
        }
        set
        {
            this.finalScoreField = value;
        }
    }

    /// <remarks/>
    public string Score
    {
        get
        {
            return this.scoreField;
        }
        set
        {
            this.scoreField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationQuitPanel
{

    private string titleField;

    private TranslationQuitPanelButtons buttonsField;

    private TranslationQuitPanelContent contentField;

    /// <remarks/>
    public string Title
    {
        get
        {
            return this.titleField;
        }
        set
        {
            this.titleField = value;
        }
    }

    /// <remarks/>
    public TranslationQuitPanelButtons Buttons
    {
        get
        {
            return this.buttonsField;
        }
        set
        {
            this.buttonsField = value;
        }
    }

    /// <remarks/>
    public TranslationQuitPanelContent Content
    {
        get
        {
            return this.contentField;
        }
        set
        {
            this.contentField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationQuitPanelButtons
{

    private string noField;

    private string yesField;

    /// <remarks/>
    public string No
    {
        get
        {
            return this.noField;
        }
        set
        {
            this.noField = value;
        }
    }

    /// <remarks/>
    public string Yes
    {
        get
        {
            return this.yesField;
        }
        set
        {
            this.yesField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class TranslationQuitPanelContent
{

    private string gameMessageField;

    private string reloadMessageField;

    private string menuMessageField;

    /// <remarks/>
    public string GameMessage
    {
        get
        {
            return this.gameMessageField;
        }
        set
        {
            this.gameMessageField = value;
        }
    }

    /// <remarks/>
    public string ReloadMessage
    {
        get
        {
            return this.reloadMessageField;
        }
        set
        {
            this.reloadMessageField = value;
        }
    }

    /// <remarks/>
    public string MenuMessage
    {
        get
        {
            return this.menuMessageField;
        }
        set
        {
            this.menuMessageField = value;
        }
    }
}

