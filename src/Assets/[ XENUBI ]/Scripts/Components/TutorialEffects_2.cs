using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using JK.Effects;
using System;

namespace Xenubi
{
	public class TutorialEffects_2 : MonoBehaviour, ITutorialEffects
	{
		private float duration;
		private int m_currentIndex;

		[SerializeField] private Image[] imgs;
		[SerializeField] private Sprite[] spts;

		public void Initialize ()
		{
		}

		public void Show(Action callback)
		{
			duration = 0.5f;

			m_currentIndex = 0;

			TryNext (callback);
		}

		public void TryNext(Action callback)
		{
			imgs [m_currentIndex].color = new Color (1, 1, 1, 0);
			imgs [m_currentIndex].sprite = spts[m_currentIndex];

			JKTween.AlphaColorImage(imgs[m_currentIndex], 1, true, duration, () => 
			{
				m_currentIndex++;

				if (m_currentIndex < imgs.Length)
				{
					TryNext(callback);
				}
				else
				{
					callback();
				}
			});
		}

		public void Finish()
		{
			duration /= 2;
		}
	}
}