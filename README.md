# README #

**Unity _4.6.0_**

This project is an educacional game about chemistry and periodic table called Xenubi.

The XeNUBi is a game intended for chemistry students who are learning about the properties of the periodic table.
The game allows students to practice their knowledge about the relationship of the properties of a chemical element and its position in the periodic table.

### Change Log ###

0.4.0 - All features, facebook integration, multilanguage.